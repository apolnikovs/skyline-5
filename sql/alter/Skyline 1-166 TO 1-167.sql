# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.166');

# ---------------------------------------------------------------------- #
# Add table "sp_geo_cells"                                               #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS `sp_geo_cells` (
  `SpGeoCellsID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `DiaryAllocationID` int(10) NOT NULL DEFAULT '0',
  `Lat1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lat2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng2` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`SpGeoCellsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='stores service provider geocell information';



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.167');
