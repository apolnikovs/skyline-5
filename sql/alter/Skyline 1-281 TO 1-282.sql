# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.281');

# ---------------------------------------------------------------------- #
# Modify appointment table												 #
# ---------------------------------------------------------------------- # 
ALTER TABLE appointment ADD PreVisitContactRequired ENUM( 'Y', 'N' ) NOT NULL DEFAULT 'N' AFTER CustContactType,
						ADD PreVisitContactInfo TEXT NULL AFTER PreVisitContactRequired;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.282');
