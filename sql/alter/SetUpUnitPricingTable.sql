--
-- Set up unit_pricing_structure table
--
CREATE TABLE IF NOT EXISTS import_unit_pricing_repair (
	Network varchar(40),
	Client varchar(40),
	ClientAccount varchar(10),
	UnitType varchar(40),
	JobSite varchar(40),
	CompletionStatus varchar(20),
	ServiceRate decimal(10,2),
	ReferralFee decimal(10,2)
);

LOAD DATA INFILE 'd:/Brian/My Documents/Netbeans Projects/SkyLine/sql/Initialise Database/RMA Import Data/UnitPricingRepair.csv' 
INTO TABLE import_unit_pricing_repair
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

INSERT INTO unit_type (UnitTypeName, RepairSkillID, CreatedDate, ModifiedUserID) 
select 'LCD/PLASMA TV 33-41INCH', t4.RepairSkillID, null, t5.UserID 
from repair_skill t4, user t5 
where t4.RepairSkillName='Brown Goods' and t5.Username='sa';

INSERT INTO unit_type (UnitTypeName, RepairSkillID, CreatedDate, ModifiedUserID) 
select 'LCD/PLASMA TV 42INCH+', t4.RepairSkillID, null, t5.UserID 
from repair_skill t4, user t5 
where t4.RepairSkillName='Brown Goods' and t5.Username='sa';

insert into unit_pricing_structure (NetworkID, ClientId, UnitTypeID, CompletionStatusID, JobSite, ServiceRate, ReferralFee, CreatedDate, ModifiedUserID)
select n.networkID, c.ClientID, ut.UnitTypeID, cs.CompletionStatusID, imp.JobSite, imp.ServiceRate, imp.ReferralFee, null, u.UserID	
from import_unit_pricing_repair imp
left join network n on n.CompanyName=imp.Network
left join client c on c.ClientName=imp.Client
left join unit_type ut on ut.UnitTypeName=imp.UnitType
left join completion_status cs on cs.CompletionStatus=imp.CompletionStatus
left join user u on u.Username='sa';