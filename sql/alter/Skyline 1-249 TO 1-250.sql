# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.249');

# ---------------------------------------------------------------------- #
# Modify sp_part_stock_item Table                                        #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_item ADD COLUMN SPPartOrderID INT(11) NULL DEFAULT NULL AFTER JobID;

# ---------------------------------------------------------------------- #
# Modify part Table                                                      #
# ---------------------------------------------------------------------- # 
ALTER TABLE part CHANGE COLUMN ModifiedUserID ModifiedUserID INT(11) NULL DEFAULT NULL AFTER SupplierOrderNo, ADD COLUMN SpPartStockTemplateID INT(11) NULL DEFAULT NULL AFTER ModifiedUserID;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.250');
