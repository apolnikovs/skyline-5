/************************************************** 
 * 
 *  Updated for SkyLine project use
 *  
 *  - global functions moved top the top of file
 * 
 *  
 *   
 **************************************************/

 /************************************************************************** 
  * 21/01/2013 1.1 Brian Etherington Added onSuccess callback 
  * with parameters mode and returned ajax data structure from update method.
  * mode = 0 for update, 1 for insert, 3 for delete
  * data = depends on the application ajax update method
  * example onSuccess: function do_something( mode, data );
  * 
  * 05/02/2013 1.2 Brian Etherington removed $.fn. namespace for colorbox v1.3.34
  * 14/02/2013 1.3 Brian Etherington added a filtering delay for server side processing
  *                                  added an ajax wait spinner to server side processing event 
  * *************************************************************************/

function urlencode (str) {
            str = (str + '').toString();
            return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } 
        
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-eu-pre": function ( date ) {
                var date = date.replace(" ", "");
          
                if (date.indexOf('.') > 0) {
                    /*date a, format dd.mn.(yyyy) ; (year is optional)*/
                    var eu_date = date.split('.');
                } else {
                    /*date a, format dd/mn/(yyyy) ; (year is optional)*/
                    var eu_date = date.split('/');
                }
          
                /*year (optional)*/
                if (eu_date[2]) {
                    var year = eu_date[2];
                } else {
                    var year = 0;
                }
          
                /*month*/
                var month = eu_date[1];
                if (month.length === 1) {
                    month = 0+month;
                }
          
                /*day*/
                var day = eu_date[0];
                if (day.length === 1) {
                    day = 0+day;
                }
          
                return (year + month + day) * 1;
            },
 
            "date-eu-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
 
            "date-eu-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
} ); 


jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
    var _that = this;
 
    if ( iDelay === undefined ) {
        iDelay = 250;
    }
      
    this.each( function ( i ) {
        $.fn.dataTableExt.iApiIndex = i;
        var $this = this;
        var oTimerId = null;
        var sPreviousSearch = null;
        var anControl = $( 'input', _that.fnSettings().aanFeatures.f );         
        anControl.unbind( 'keyup' ).bind( 'keyup', function() {
            var $$this = $this;
  
            if (sPreviousSearch === null || sPreviousSearch !== anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val(); 
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter( anControl.val() );
                }, iDelay);
            }
        } );
          
        return this;
    } );
    return this;
};

jQuery.fn.dataTableExt.oApi.fnFilterOnReturn = function (oSettings) {
    /*
     * Usage:       $('#example').dataTable().fnFilterOnReturn();
     * Author:      Jon Ranes (www.mvccms.com)
     * License:     GPL v2 or BSD 3 point style
     * Contact:     jranes /AT\ mvccms.com
     */
    var _that = this;

    this.each(function (i) {
        $.fn.dataTableExt.iApiIndex = i;
        var $this = this;
        var anControl = $('input', _that.fnSettings().aanFeatures.f);
        anControl.unbind('keyup').bind('keypress', function (e) {
            if (e.which === 13) {
                $.fn.dataTableExt.iApiIndex = i;
                _that.fnFilter(anControl.val());
            }
        } );
        return this;
     } );
     return this;
};

var oTable = new Array();

(function($) {
    /* Get the rows which are currently selected */
    function fnGetSelected(oTableLocal) {
        var aReturn = new Array();
        var aTrs = oTableLocal.fnGetNodes();

        for ( var i=0 ; i<aTrs.length ; i++ ) {
            if ( $(aTrs[i]).hasClass('row_selected') ) {
                aReturn.push( aTrs[i] );
            }
        }
        return aReturn;
    }

   //This is  used for to generate url encode of given string
    function urlencodeData (str) {
            str = (str + '').toString();
            return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } 


    function info() {
	var _info = new Array('SkyLine');
	return _info;
    }

    

    function updateHeight($eId) {
        
        if(location.href != top.location.href){ 
            // the content has been loaded into an IFRAME
            // so tell parent to resize IFRAME
            $.postMessage(
            
                    'if_height=' + $('body').outerHeight(true),
                    options['subdomain']+'/product/',
                    parent
            );
                
             
        }
        else
            {
                 if($.colorbox)
                 {
                        if($eId!='' && $eId)
                        {
                            //$.colorbox.resize({
                            //    height: ($('#'+$eId).height()+150)+"px"
                            //});
			    $.colorbox.resize();
                        } 
                        else
                            {
                                 $.colorbox.resize();
                            }
                }
            }
        
    }

   function prepareId($arg1, $arg2)
   {
       if($arg1=='')
           {
               $arg1 = $arg2;
           }
       return $arg1;
   }

  
   //var oTable = new Array();

   $.fn.PCCSDataTable = function(optionsOrg) {  

    // Create some defaults, extending them with any options that were provided

    var $orgHtmlTableId = '';
    if(optionsOrg['htmlTableId']!='' && optionsOrg['htmlTableId'])
        {
            $orgHtmlTableId = optionsOrg['htmlTableId'];
        }

    var options = $.extend({
        
                        
                        displayButtons:'', // Write A for Add, U for Update, D for Delete, V for view and P Pick...Ex: AUD
                        htmlTableId: 'myBrowse',
                        htmlTablePageId: 'myBrowsePageId',
                        addButtonId:    $orgHtmlTableId+'addButtonId',
                        updateButtonId: $orgHtmlTableId+'updateButtonId',
                        deleteButtonId: $orgHtmlTableId+'deleteButtonId',
                        viewButtonId:   $orgHtmlTableId+'viewButtonId',
                        pickButtonId:   $orgHtmlTableId+'pickButtonId',
                        
                        formInsertButton: $orgHtmlTableId+'formInsertButton',
                        formUpdateButton: $orgHtmlTableId+'formUpdateButton',
                        formDeleteButton: $orgHtmlTableId+'formDeleteButton',
                        formCancelButton: $orgHtmlTableId+'formCancelButton',
                        formViewButton: $orgHtmlTableId+'formViewButton',
                        
                        aaSorting:    [[ 0, "asc" ]],
                        
                        fetchDataUrl:  '',
                        updateDataUrl: '',
                        createDataUrl: '',
                        deleteDataUrl: '',
                        createAppUrl:  '',
                        updateAppUrl:  '',
                        viewAppUrl:    '',
                        deleteAppUrl:  '',
                        
                        onSuccess: null,
                        
                        colorboxFormId: $orgHtmlTableId+'colorboxFormId',
                        parentURL: false,
                        
                        
                        popUpFormWidth: 0, //Put 0 for default width
                        popUpFormHeight: 600, //Put 0 for default height
                        
                        frmErrorRules: {},
                        frmErrorMessages: { },
                        frmErrorMsgClass: 'webformerror',
                        frmErrorElement: 'label',
                        
                        isExistsDataUrl: false,
                        suggestTextId: $orgHtmlTableId+'suggestTextId',
                        sugestFieldId: $orgHtmlTableId+'sugestFieldId',
                        frmErrorSugMsgClass: 'suggestwebformerror',
                        
                        hiddenPK: false,
                        passParamsType:'0',
                        superFilter: {},
                        subdomain: window.location,
                        searchCloseImage:'../css/Skins/skyline/images/close.png',
                       
                       
                        pickButtonAlwaysEnabled: false,
                        colorboxForceClose:true,
                        bServerSide:true,
                        bProcessing:true,
                        bDeferRender:false,
                        sDom: 'ft<"#dataTables_command">rpli',
                        sPaginationType:'full_numbers',
                        iDisplayLength: 50,
                        addButtonText:'Add',
                        createFormTitle:'Add new record',
                        updateButtonText:'Update',
                        updateFormTitle:'Update record',
                        deleteButtonText:'Delete',
                        deleteFormTitle: 'Delete record',
                        viewButtonText:'View',
                        viewFormTitle: 'View record',
                        pickButtonText:'Select',
                        searchBoxLabel: 'Search within results: ',
                        pickCallbackMethod: '',
                        dblclickCallbackMethod: '',
                        pickButtonStyle: '',
                        fnRowCallback: '',
			fnDrawCallback: '',
                        fnCreatedRow:'',
			
			fnInintComplete: '',
			
                        tooltipTitle:'',
                        dataDeletedMsgId:  "dataDeletedMsg",
                        dataInsertedMsgId: "dataInsertedMsg",
                        dataUpdatedMsgId:  "dataUpdatedMsg",
                        copyInsertRowId:'',
                        formDataErrorMsgId:'',
                        createFormFocusElementId:'',
                        updateFormFocusElementId:'',
                        insertBeforeSendMethod: '',
                        updateBeforeSendMethod: '',
                        bottomButtonsDivId:'dataTables_command',
                        displayProcessingTextId:'',
                        displayProcessingText:'',
                        hidePaginationNorows: false,
                        sScrollY: '',
                        bScrollCollapse: false,
                        bPaginate: true,
                        htmlChildTableId: false,
                        bDestroy: false,
                        bStateSave: false,
                        oColReorder:{ "aiOrder": [3,2,1]}

                        

                    } , optionsOrg);
                          
        oTable[options['htmlTableId']] = false;
        
        var passParamsTypeStr;
        
        //Primary key parmater passing type.
        if(options['passParamsType']=='1') {
               passParamsTypeStr = "?pk="; 
        } else {
               passParamsTypeStr = ""; 
        }                 

       
        
        
         //Assing false to button ids if button id value is ''
        options['htmlTablePageId'] = prepareId(options['htmlTablePageId'], options['htmlTableId']+"PageId"); 
        options['addButtonId'] = prepareId(options['addButtonId'], options['htmlTableId']+"addButtonId");
        options['updateButtonId'] = prepareId(options['updateButtonId'], options['htmlTableId']+"updateButtonId");
        options['deleteButtonId'] = prepareId(options['deleteButtonId'], options['htmlTableId']+"deleteButtonId");
        options['viewButtonId'] = prepareId(options['viewButtonId'], options['htmlTableId']+"viewButtonId");
        options['pickButtonId'] = prepareId(options['pickButtonId'], options['htmlTableId']+"pickButtonId");
        options['formInsertButton'] = prepareId(options['formInsertButton'], options['htmlTableId']+"formInsertButton");
        options['formUpdateButton'] = prepareId(options['formUpdateButton'], options['htmlTableId']+"formUpdateButton");
        options['formDeleteButton'] = prepareId(options['formDeleteButton'], options['htmlTableId']+"formDeleteButton");    
        options['formCancelButton'] = prepareId(options['formCancelButton'], options['htmlTableId']+"formCancelButton");    
        options['formViewButton'] = prepareId(options['formViewButton'], options['htmlTableId']+"formViewButton");    
        
        options['colorboxFormId'] = prepareId(options['colorboxFormId'], options['htmlTableId']+"colorboxFormId"); 
        options['suggestTextId'] = prepareId(options['suggestTextId'], options['htmlTableId']+"suggestTextId");       
        options['sugestFieldId'] = prepareId(options['sugestFieldId'], options['htmlTableId']+"sugestFieldId");  
        options['bottomButtonsDivId'] = prepareId(options['bottomButtonsDivId'], options['htmlTableId']+"dataTables_command");  
        
        options['sDom'] = prepareId(options['sDom'], 'ft<"#'+options['bottomButtonsDivId']+'">rpli');  
        
        options['sPaginationType'] = prepareId(options['sPaginationType'], 'full_numbers');  
        
               
               
             
        
        
      //If user provides fetch data url then we are assigning true values to the following options.  
      if(options['fetchDataUrl']!='' && options['fetchDataUrl'])
      {
          
            if(options['bServerSide'])
                {
            options['bServerSide']  = true;  
                }
            
            if( options['bProcessing'])
                {
            options['bProcessing']  = true;  
                }
                
            if(options['bDeferRender'])
                {
            options['bDeferRender']  = true;  
      }
      }
        
    
        

    return this.each(function() {    
            
         
       $('#'+options['htmlTableId']+' tbody').mouseover( function() {
           
           this.setAttribute( 'title', options['tooltipTitle'] );
           
        } );    
            
            
            
        //Double click action on data table row - starts here..    
        $('#'+options['htmlTableId']+' tbody').dblclick(function(event) {
            
                if(options['dblclickCallbackMethod'] && options['dblclickCallbackMethod']!='')
                {
                    
                     $(oTable[options['htmlTableId']].fnSettings().aoData).each(function () {
                        $(this.nTr).removeClass('row_selected');
                    } );
                    
                    $(event.target.parentNode).addClass('row_selected');
                    
                    var sltdRowData = new Array();
                    var nTds = $('td', event.target.parentNode);


                    for($i=0;$i<nTds.length;$i++)
                    {
                        sltdRowData[$i] = $(nTds[$i]).text();
                    }
                    
                    eval(options['dblclickCallbackMethod']+'(sltdRowData);');
                } 
           
        }); 
       //Double click action on data table row - ends here..     
            
            
            
        /* Add a click handler to the rows - this could be used as a callback */
        $('#'+options['htmlTableId']+' tbody').click(function(event) {
            
             
            if ($(event.target.parentNode).hasClass('row_selected') || (event.target.parentNode.tagName.toLowerCase()=="td" && $(event.target.parentNode.parentNode).hasClass('row_selected'))) {
                
                    if(event.target.parentNode.tagName.toLowerCase()=="td")
                    {
                         $(event.target.parentNode.parentNode).removeClass('row_selected');
                    }
                    else
                    {
                          $(event.target.parentNode).removeClass('row_selected');   
                    }
               
                    if(options['updateButtonId']!='' && options['updateButtonId'])
                        {
                            $('#'+options['updateButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }

                    if(options['deleteButtonId']!='' && options['deleteButtonId'])
                        {
                                $('#'+options['deleteButtonId']).attr('disabled','disabled').removeClass('gplus-red').addClass('gplus-red');
                        }   

                    if(options['viewButtonId'] && options['viewButtonId']!='')
                        {
                            $('#'+options['viewButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }    
                    if(options['pickButtonId'] && options['pickButtonId']!='' && options['pickButtonAlwaysEnabled']!=true)
                        {
                                $('#'+ options['pickButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }    
            
            } else {
                
                $(oTable[options['htmlTableId']].fnSettings().aoData).each(function () {
                    $(this.nTr).removeClass('row_selected');
                } );
                
                
                   if(event.target.parentNode.tagName.toLowerCase()=="td")
                   {
                        $(event.target.parentNode.parentNode).addClass('row_selected');
                   }
                   else
                   {
                        $(event.target.parentNode).addClass('row_selected');
                   }
                
                
                
                if(oTable[options['htmlTableId']].fnSettings().aiDisplay.length>0)
                {
                    if(options['updateButtonId'] && options['updateButtonId']!='')
                        {
                            $('#'+options['updateButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                        } 

                    if(options['deleteButtonId']!='' && options['deleteButtonId'])
                        {
                            $('#'+options['deleteButtonId']).removeAttr('disabled').removeClass('gplus-red-disabled').addClass('gplus-red');
                        }

                    if(options['viewButtonId'] && options['viewButtonId']!='')
                        {
                                $('#'+options['viewButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                        }

                    if(options['pickButtonId'] && options['pickButtonId']!='')
                        {
                                $('#'+ options['pickButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                        }
                }
            
            }            
        } );
        
        
        $(document).on('click', '#'+options['htmlTableId']+'_filter img',  function(){
            $(this).parent().find('input').val('').keyup();
        });

        /* Add a click handler to the add button of datatable */
        $(document).on('click', '#'+options['addButtonId'], 
            function() {

                

                // Ignore action if button disabled
                if (!$('#'+options['addButtonId']).attr('disabled')) {

                              
                                
                                if(options['createAppUrl']!='' && options['createAppUrl'])
                                {
                                    
                                    var $createAppUrl =  options['createAppUrl'];
                                    if(options['copyInsertRowId']!='' && options['copyInsertRowId'])
                                        {
                                           $createAppUrl = $createAppUrl+$('#'+options['copyInsertRowId']).val();
                                        }
                                        
                                    
                                    //It opens color box popup page.              
                                    $.colorbox( { href: $createAppUrl,
                                                    title: options['createFormTitle'],
                                                    opacity: 0.75,
                                                    height:options['popUpFormHeight'],
                                                    width:options['popUpFormWidth'],
                                                    overlayClose: false,
                                                    escKey: false,
                                                    onComplete: function(){
                                                                   updateHeight('');
                                                                    if(options['createFormFocusElementId']!='' && options['createFormFocusElementId'])
                                                                    {
                                                                        $('#'+options['createFormFocusElementId']).focus();
                                                                    }
                                                                }
                                            } );
                                }
            }      
                        }      
        );
            
        


                    /* Add a click handler to the update button of datatable */
                    $(document).on('click', '#'+options['updateButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['updateButtonId']).attr('disabled')) {
                                    
                                    
                                    var anSelected = fnGetSelected( oTable[options['htmlTableId']] );

                                    if(options['updateAppUrl'] && options['updateAppUrl']!='')
                                        {
                                            var $selectedRowId = oTable[options['htmlTableId']].fnGetData(anSelected[0],0);
                                            //It opens color box popup page.  
                                            if($selectedRowId && $selectedRowId!='')
                                            {   
                                                $.colorbox( { href:options['updateAppUrl']+passParamsTypeStr+urlencodeData($selectedRowId),
                                                            title: options['updateFormTitle'],
                                                            height:options['popUpFormHeight'],
                                                            width:options['popUpFormWidth'],
                                                            opacity: 0.75,     
                                                            overlayClose: false,
                                                            escKey: false,
                                                                    onComplete: function(){
                                                                        if(options['sugestFieldId'] && options['sugestFieldId']!='')
                                                                            {
                                                                                $('#'+options['sugestFieldId']).attr('disabled', 'disabled'); 
                                                                            }
                                                                        //commented out, because otherwise it will resize colorbox
									//twice when opening
									
									updateHeight('');
                                                                        
                                                                        if(options['updateFormFocusElementId']!='' && options['updateFormFocusElementId'])
                                                                        {
                                                                            $('#'+options['updateFormFocusElementId']).focus();
                                                                        }
                                                                    }
                                                    } );
                                            }
                                        }
                                }      
                            }      
                        );



                        /* Add a click handler to the delete button of datatable */   
                        $(document).on('click', '#'+options['deleteButtonId'], 
                            function() {
                                
                                if(options['deleteButtonId']!='' && options['deleteButtonId'])
                                    {
                                        // Ignore action if button disabled
                                        if (!$('#'+options['deleteButtonId']).attr('disabled')) {


                                        var anSelected = fnGetSelected( oTable[options['htmlTableId']] );

                                            if(options['deleteAppUrl'] && options['deleteAppUrl']!='')
                                                {
                                                    //It opens color box popup page. 
                                                    $.colorbox( { href:options['deleteAppUrl']+passParamsTypeStr+urlencodeData(oTable[options['htmlTableId']].fnGetData(anSelected[0],0)),
                                                                title: options['deleteFormTitle'],
                                                                opacity: 0.75,     
                                                                overlayClose: false,
                                                                escKey: false,
                                                                onComplete: function(){
                                                                   updateHeight('');
                                                                }
                                                            } );
                                                }


                                        }
                                
                                 }
                            }      
                        );    


                          /* Add a click handler to the pick data button of datatable */   
                        $(document).on('click', '#'+options['pickButtonId'], 
                            function() {
                                
                                // Ignore action if button disabled
                                if (!$('#'+options['pickButtonId']).attr('disabled')) {

                                   
                                        if(options['pickCallbackMethod'] && options['pickCallbackMethod']!='')
                                        {
                                            
                                            var anSelected = fnGetSelected( oTable[options['htmlTableId']] );
                                            
                                            eval(options['pickCallbackMethod']+'(oTable[options["htmlTableId"]].fnGetData(anSelected[0]));');
                                            
                                            
                                            if(options['colorboxForceClose'] && options['colorboxForceClose']!='')
                                            {
                                                $('#'+options['pickButtonId']).die('click');
                                                $.colorbox.close();
                                            }
                                        }
                                    
                                }
                            }      
                        );
                            
                            
                            
                            
                        /* Add a click handler to the view button of datatable */   
                        $(document).on('click', '#'+options['viewButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['viewButtonId']).attr('disabled')) {

                                
                                    var anSelected = fnGetSelected( oTable[options['htmlTableId']] );

                                    if(options['viewAppUrl'] && options['viewAppUrl']!='')
                                        {
                                            //It opens color box popup page. 
                                            $.colorbox( { href:options['viewAppUrl']+passParamsTypeStr+urlencodeData(oTable[options['htmlTableId']].fnGetData(anSelected[0],0)),
                                                        title: options['viewFormTitle'],
                                                        opacity: 0.75,     
                                                        overlayClose: false,
                                                        escKey: false,
                                                        onComplete: function(){
                                                                   updateHeight('');
                                                                }
                                                    
                                                } );
                                        }

                                
                                 }      
                            }      
                        );                             




                        if(location.href != top.location.href){ 
                            // the content has been loaded into an IFRAME
                            // control full screen overlay in parent window when colorbox is opened/closed
                            $(document).bind('cbox_open', function() {
                                    
                                    if(options['parentURL']!='' && options['parentURL'])
                                        {
                                            $.postMessage(
                                                'showModalOverlay',
                                                options['parentURL'],
                                                parent
                                            );
                                        }  
                                } );
                            $(document).bind('cbox_closed', function() {
                                    
                                    if(options['parentURL']!='' && options['parentURL'])
                                        {
                                            $.postMessage(
                                                'hideModalOverlay',
                                                options['parentURL'],
                                                parent
                                            );
                                        }
                                        
                                } );
                        }



                        /* Add a click handler to the delete button of colorbox popup form */  
                        $(document).on('click', '#'+options['formDeleteButton'], 
                            function() {

                                if(options['deleteDataUrl']!='' && options['deleteDataUrl'])
                                    {
                                        $('*').css('cursor','wait');
                                        $.post(options['deleteDataUrl'],        

                                            $("#"+options['colorboxFormId']).serialize(),      
                                            function(data){
                                                // DATA NEXT SENT TO COLORBOX
                                                var p = eval("(" + data + ")");
                                                
                                                
                                                    if(p['status']=="ERROR")
                                                    {
                                                        if(options['formDataErrorMsgId']!='' && options['formDataErrorMsgId'])
                                                        {
                                                            //alert(p['message']);
                                                            $('#'+options['formDataErrorMsgId']).addClass(options['frmErrorSugMsgClass']);
                                                            $('#'+options['formDataErrorMsgId']).html(p['message']);

                                                        }

                                                    }
                                                    else if(p['status']=="OK")
                                                    {
                                                                                                                                      // OnSuccess Callback
                                                            if (options['onSuccess'] !== null) {
                                                                options['onSuccess'](3, p);
                                                            }
                                                           
                                                            var infoMsg;
                                                            if($("#"+options['dataDeletedMsgId']).length>0)
                                                                {
                                                                    infoMsg = $("#"+options['dataDeletedMsgId']).html();
                                                                }
                                                                else
                                                                {
                                                                    infoMsg = p['message'];
                                                                }


                                                                $.colorbox({
                                                                    title: options['deleteFormTitle'],
                                                                    html:   infoMsg,
                                                                    fixed:true,
                                                                    height:options['popUpFormHeight'],
                                                                    width:options['popUpFormWidth'],
                                                                    onComplete: function(){
                                                                        updateHeight('');
                                                                     },
                                                                    onClosed:function(){ oTable[options['htmlTableId']].fnDraw(true); } 
                                                                }); 
                                                                
                                                    } 
                                                    $('*').css('cursor','default');
                                            }); 
                                    }
                                return false;

                            }
                            );
                                
                                
                

                        /* Add a click handler to the update button of colorbox popup form */  
                    $(document).on('click', '#'+options['formUpdateButton'], 
                            function() {
                                
                                
                                
                                //Validating and posting form data.
                                $('#'+options['colorboxFormId']).validate({
                                            rules: options['frmErrorRules'],
                                            messages: options['frmErrorMessages'],               
                                            ignore: '',
                                            errorClass: options['frmErrorMsgClass'],
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: options['frmErrorElement'],

                                            submitHandler: function() {
                                                
                                                   $('*').css('cursor','wait');

                                                    $('#'+options['formUpdateButton']).hide();
                                                    $('#'+options['formCancelButton']).hide();
                                                
                                                    $("[disabled='disabled']").removeAttr("disabled");
                                                    
                                                       
                                                        
                                                        if(options['updateDataUrl']!='' && options['updateDataUrl'])
                                                        {
                                                            
                                                            if(options['updateBeforeSendMethod']!='' && options['updateBeforeSendMethod'])
                                                            {
                                                                $postFormDelay = 1000;

                                                                eval('$postFormFlag  = '+options['updateBeforeSendMethod']+'();');

                                                            }
                                                            else
                                                            {
                                                                $postFormDelay = 0;   
                                                            }
                                                            
                                                             if(options['htmlChildTableId'])
                                                             {    
                                                                var sChildData = "&"+jQuery('input', oTable[options['htmlChildTableId']].fnGetNodes()).serialize();
                                                             }
                                                             else
                                                             {
                                                                var sChildData = '';     
                                                             }
                                                             
                                                             $(window).delay($postFormDelay).queue(function() {
                                                            
                                                                    $.post(options['updateDataUrl'],        

                                                                    $("#"+options['colorboxFormId']).serialize()+sChildData,      
                                                                    function(data){

                                                                        // DATA NEXT SENT TO COLORBOX
                                                                        var p = eval("(" + data + ")");

                                                                        if(p['status']=="ERROR")
                                                                            {
                                                                                if(options['formDataErrorMsgId']!='' && options['formDataErrorMsgId'])
                                                                                {

                                                                                    $('#'+options['formDataErrorMsgId']).addClass(options['frmErrorSugMsgClass']);
                                                                                    $('#'+options['formDataErrorMsgId']).html(p['message']);

                                                                                }
                                                                               
                                                                            }
                                                                            else if(p['status']=="OK")
                                                                            {
                                                                                // OnSuccess Callback
                                                                                if (options['onSuccess'] !== null) {
                                                                                    options['onSuccess'](0, p);
                                                                                }
                                                                               
                                                                                var infoMsg;
                                                                                if($("#"+options['dataUpdatedMsgId']).length>0)
                                                                                    {
                                                                                        infoMsg = $("#"+options['dataUpdatedMsgId']).html();
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        infoMsg = p['message'];
                                                                                    }                                                                                  

                                                                                $.colorbox({

                                                                                    title: options['updateFormTitle'],
                                                                                    html:  infoMsg,
                                                                                    fixed:true,
                                                                                    width:options['popUpFormWidth'],
                                                                                    onClosed:function(){ oTable[options['htmlTableId']].fnDraw(true); } 
                                                                                    }); 

                                                                            }
                                                                            
                                                                            $('#'+options['formUpdateButton']).show();
                                                                            $('#'+options['formCancelButton']).show();
                                                                            $('*').css('cursor','default');

                                                                    });//Post ends here..
                                                                    
                                                                    $(this).dequeue();
                                                                    
                                                               });//set delay ends here...      
                                                    
                                                        }

                                            }
                                });



                            }
                            );



                     //keyup sugestFieldId for check ID
                    $(document).on('keyup', '#'+options['sugestFieldId'], 
                            function() {

                                    if(options['hiddenPK']!='' && options['hiddenPK']) {
                                        
                                            if(!$('#'+options['hiddenPK']).val()) {

                                                        if(options['sugestFieldId']!='' && options['sugestFieldId'])
                                                        {
                                                                $unique_txt = $('#'+options['sugestFieldId']).val();

                                                                if(options['isExistsDataUrl'] && options['isExistsDataUrl']!='')
                                                                    {
                                                                            $.post(options['isExistsDataUrl'],{pk:$unique_txt},function(result){

                                                                                   if(options['suggestTextId']!='' && options['suggestTextId'])
                                                                                       {
                                                                                            if(result=="null") {        

                                                                                                $('#'+options['suggestTextId']).html(null);
                                                                                                $('#'+options['suggestTextId']).removeClass(options['frmErrorSugMsgClass']);
                                                                                            } 
                                                                                            else 
                                                                                            {
                                                                                                $('#'+options['suggestTextId']).addClass(options['frmErrorSugMsgClass']);
                                                                                                $('#'+options['suggestTextId']).html('The '+options['sugestFieldId']+' you have entered is already exists in our database.');

                                                                                            }
                                                                                       }
                                                                        });
                                                                    }
                                                    }                     
                                        }
                                
                                }

                            });


                        /* Add a click handler to the insert button of colorbox popup form */  

                    $(document).on('click', '#'+options['formInsertButton'], function() {

                       
                        //delay(2000);
                        
                        
                       // $("#UploadedBrandLogo").val().delay(2000);

                        //Validating and posting form data.
                        $('#'+options['colorboxFormId']).validate({
                            rules: options['frmErrorRules'],
                            messages: options['frmErrorMessages'],               
                            ignore: '',
                            errorClass: options['frmErrorMsgClass'],
                            onkeyup: false,
                            onblur: false,
                            errorElement: options['frmErrorElement'],

                            submitHandler: function() {
                                
                                $('*').css('cursor','wait');

                                 // $('#'+options['formInsertButton']).val("Processing…");
                                  $('#'+options['formInsertButton']).hide();
                                  $('#'+options['formCancelButton']).hide();
                                  
                               //  $('#'+options['displayProcessingTextId']).val("Processing…");
                                 
                                 
                                 if(options['displayProcessingTextId']!='' && options['displayProcessingText']!='')
                                     {
                                          $('#'+options['formInsertButton']).hide();
                                          $('#'+options['displayProcessingTextId']).html(options['displayProcessingText']+"&hellip;");
                                     }
                                 
                                 
                                  
                                   $("[disabled='disabled']").removeAttr("disabled");
                                    
                                    if(!$('#'+options['suggestTextId']).html()) {
                                        
                                        if(options['createDataUrl']!='' && options['createDataUrl'])
                                            {
                                                
                                                if(options['insertBeforeSendMethod']!='' && options['insertBeforeSendMethod'])
                                                {
                                                    $postFormDelay = 1000;
                                                    
                                                    eval('$postFormFlag  = '+options['insertBeforeSendMethod']+'();');
                                                    
                                                }
                                                else
                                                {
                                                     $postFormDelay = 0;   
                                                }
                                                 
                                                 
                                                    if(options['htmlChildTableId'])
                                                    {    
                                                       var sChildData = "&"+jQuery('input', oTable[options['htmlChildTableId']].fnGetNodes()).serialize();
                                                    }
                                                    else
                                                    {
                                                       var sChildData = '';     
                                                    }
                                                 
                                                
                                                    $(window).delay($postFormDelay).queue(function() {

                                                        $.post(options['createDataUrl'],        

                                                        $("#"+options['colorboxFormId']).serialize()+sChildData,      
                                                        function(data){
                                                            // DATA NEXT SENT TO COLORBOX
                                                            $('*').css('cursor','default');
							    var p = eval("(" + data + ")");

                                                                if(p['status']=="ERROR")
                                                                    {
                                                                        if(options['formDataErrorMsgId']!='' && options['formDataErrorMsgId'])
                                                                        {

                                                                            $('#'+options['formDataErrorMsgId']).addClass(options['frmErrorSugMsgClass']);
                                                                            $('#'+options['formDataErrorMsgId']).html(p['message']);

                                                                        }
                                                                       // document.body.style.cursor='default';
                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {
                                                                        
                                                                        // OnSuccess Callback
                                                                        if (options['onSuccess'] !== null) {
                                                                            options['onSuccess'](1, p);
                                                                        }

                                                                        if(options['formDataErrorMsgId']!='' && options['formDataErrorMsgId'])
                                                                        {

                                                                            $('#'+options['formDataErrorMsgId']).html(null);
                                                                            $('#'+options['formDataErrorMsgId']).removeClass(options['frmErrorSugMsgClass']);

                                                                        }
                                                                        
                                                                        
                                                                        var infoMsg;
                                                                        if($("#"+options['dataInsertedMsgId']).length>0)
                                                                            {
                                                                                infoMsg = $("#"+options['dataInsertedMsgId']).html();
                                                                            }
                                                                            else
                                                                            {
                                                                                infoMsg = p['message'];
                                                                            }


                                                                        $.colorbox({
                                                                            title: options['createFormTitle'],
                                                                            html:  infoMsg,
                                                                            fixed:true,
                                                                            width:options['popUpFormWidth'],
                                                                            onClosed:function(){ oTable[options['htmlTableId']].fnDraw(true); } 
                                                                        }); 
                                                                    }
                                                                    
                                                                    
                                                                     $('#'+options['formInsertButton']).show();
                                                                     $('#'+options['formCancelButton']).show();
                                                                                                                                        
                                                        }); //Post ends here...
                                                        
                                                        
                                                        $(this).dequeue();
                                                        
                                                  });//set delay ends here...
                                               
                                            }
                                        
                                    }// end if

                                }// end submitHandler
                       }); // validate
                    });



                        /* Add a click handler to the cancel button of colorbox popup form */  
                        $(document).on('click', '#'+options['formCancelButton'], 
                            function() {
                                $.colorbox.close();
                            }      
                        );


                          /* Add a click handler to the view button of colorbox popup form */  
                        $(document).on('click', '#'+options['formViewButton'], 
                            function() {
                                $.colorbox.close();
                            }      
                        );          

                        oTable[options['htmlTableId']] = $('#' + options['htmlTableId']).dataTable({
                           "fnServerParams" : options['fnServerParams'],
                           "aoColumns": options['aoColumns'],
                           "aaSorting": options['aaSorting'],                           
                           'sDom': options['sDom'],
                           'sPaginationType': options['sPaginationType'],
                            "iDisplayLength": options['iDisplayLength'],
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ entries",
                                "sSearch": ""+options['searchBoxLabel']+"",
                                "sProcessing": 'Please wait...<img src="'+_SUBDOMAIN+'/images/ajax-loader.gif" style="margin:5px 0 0 5px;"/>'
                            },
                            fnDrawCallback: function() {
				
                                updateHeight(options['htmlTablePageId']);
                               
                                $('#'+options['bottomButtonsDivId']).html(function(){
                                    
				    var htmlButtons = new Array(); 
                                    
                                    if(options['addButtonId']!='' && options['addButtonId'])
                                    {
                                        htmlButtons['A'] = '<button id="'+options['addButtonId']+'" type="button" class="gplus-blue"><span class="label">'+options['addButtonText']+'</span></button>';
                                    }
                                    
                                    if(options['updateButtonId'] && options['updateButtonId']!='')
                                    {
                                        htmlButtons['U'] = '<button id="'+options['updateButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label" >'+options['updateButtonText']+'</span></button>';
                                    }
                                    
                                    if(options['deleteButtonId'] && options['deleteButtonId']!='')
                                    {
                                        htmlButtons['D'] = '<button id="'+ options['deleteButtonId']+'" type="button" class="gplus-red" disabled="disabled"><span class="label">'+options['deleteButtonText']+'</span></button>';
                                    }
                                 
                                    if(options['viewButtonId'] && options['viewButtonId']!='')
                                    {
                                        htmlButtons['V'] = '<button id="'+ options['viewButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">'+options['viewButtonText']+'</span></button>';
                                    }
                                        
                                    if(options['pickButtonId'] && options['pickButtonId']!='')
                                    {
                                        
                                        if(options['pickButtonAlwaysEnabled']!=true)
                                        {
                                              htmlButtons['P'] = '<button id="'+ options['pickButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label" '+options['pickButtonStyle']+'  >'+options['pickButtonText']+'</span></button>';  
                                        }
                                        else
                                        {
                                                     htmlButtons['P'] = '<button id="'+ options['pickButtonId']+'" type="button" class="gplus-blue" ><span class="label" '+options['pickButtonStyle']+' >'+options['pickButtonText']+'</span></button>';  
                                        }
                                        
                                        
                                    }
                                        
                                        
                                    var html_buttons_str = ''; 
                                    
                                    for (var $hB = 0; $hB < options['displayButtons'].length; $hB++) {
                                    
                                            if(htmlButtons[options['displayButtons'].charAt($hB)])
                                            {
                                                html_buttons_str += htmlButtons[options['displayButtons'].charAt($hB)];
                                            }    
                                    }    
                                      
                                    
                                    return html_buttons_str;
                                });  
                                
                               
                                //alert(oTable.fnSettings().aiDisplay.length);
                                 
                                if($('#'+options['bottomButtonsDivId']).html()=='' || !$('#'+options['bottomButtonsDivId']).html())
                                {
                                    
                                    
                                     if(options['sPaginationType']=="full_numbers")
                                     {  
                                        
                                         
                                        if ( $('#'+options['htmlTableId']+'_paginate .paginate_button').size()>4) 
                                        {

                                                if($('#'+options['htmlTableId']+'_paginate').length > 0){

                                                    $('#'+options['htmlTableId']+'_paginate')[0].style.display = "block";

                                                }

                                                if ($('#'+options['htmlTableId']+'_length').length > 0){

                                                    $('#'+options['htmlTableId']+'_length')[0].style.display = "block";

                                                }

                                                if ($('#'+options['htmlTableId']+'_info').length > 0){

                                                    $('#'+options['htmlTableId']+'_info')[0].style.display = "block";

                                                }


                                        } 
                                        else 
                                        {
					    
                                            if($('#'+options['htmlTableId']+'_paginate').length > 0){

                                                    $('#'+options['htmlTableId']+'_paginate')[0].style.display = "block";

                                                }

                                                if ($('#'+options['htmlTableId']+'_length').length > 0){

                                                    $('#'+options['htmlTableId']+'_length')[0].style.display = "block";

                                                }

                                                if ($('#'+options['htmlTableId']+'_info').length > 0){

                                                    $('#'+options['htmlTableId']+'_info')[0].style.display = "block";

                                                }
                                        }
                                        
                                    }   
                                    else 
                                    {
                                        
                                            if(options['hidePaginationNorows'])
                                            {
                                              
                                                
                                                if($('#'+options['htmlTableId']+"_previous").hasClass("paginate_enabled_previous") || $('#'+options['htmlTableId']+"_next").hasClass("paginate_enabled_next"))
                                                {
                                                    $('#'+options['htmlTableId']+'_paginate').show();
                                                }    
                                                else //Hiding pagenavigation buttons if they are disabled and only if the hidePaginationNorows variable value true.
                                                {
                                                   
                                                     $('#'+options['htmlTableId']+'_paginate').hide();
                                                }
                                              
                                            }
                                    }

                               }
			       
                            },
                            
                            
                            fnRowCallback: function(nRow, aData, iDisplayIndex) {
				
				//Append the grade to the default row class name
				if(options['fnRowCallback'] != '' && options['fnRowCallback']) {
				    eval(options['fnRowCallback'] + '(nRow, aData);'); 
				}
                                  
                                return nRow;  
                                   
                            },
                            
                            fnCreatedRow: function(nRow, aData, iDisplayIndex) {
				
				//Append the grade to the default row class name
				if(options['fnCreatedRow'] != '' && options['fnCreatedRow']) {
				    eval(options['fnCreatedRow'] + '(nRow, aData);'); 
				}
                                  
                                return nRow;  
                                   
                            },
                            
                            
				
			    fnInitComplete: function(oSettings, aData) {
				//console.log($(this).attr("id"));
				if(options['fnInitComplete'] != '' && options['fnInitComplete']) {
                                     eval(options['fnInitComplete'] + '(oSettings, aData);'); 
				}
				
			    },
			    
                            'bServerSide': options['bServerSide'],
                            'bStateSave': options['bStateSave'],                            
                            'bProcessing': options['bProcessing'],
                            'bDeferRender': options['bDeferRender'],
                            'bFilter': true,
                            'sAjaxSource': options['fetchDataUrl'],
                            'sScrollY': options['sScrollY'],
                            'bScrollCollapse': options['bScrollCollapse'],
                            'bPaginate': options['bPaginate'],
                            'bDestroy': options['bDestroy'],                            

                            'fnServerData': function ( sSource, aoData, fnCallback ) {
                                                
                                               
                                                
                                                 if(sSource)
                                                 { 
                                                    //unserialize aoData
                                                    if(options.superFilter != '')
                                                        aoData = $.param(aoData, true) + '&' + $.param(options.superFilter, true);


                                                    $.ajax( { 'dataType': 'json', 
                                                            'type': 'POST', 
                                                            'cache': false,
                                                            'url': sSource, 
                                                            'data': aoData, 
                                                            'success': fnCallback,
                                                            'complete': function() {  updateHeight(options['htmlTablePageId']); } 
                                                    } );
                                                }

                                            }

                        } );
                        
                        if (options['bServerSide']) {
                            oTable[options['htmlTableId']].fnSetFilteringDelay(350);
                        }
                        
                        if(options['searchCloseImage']!='' && options['searchCloseImage']) {
                                $('#'+options['htmlTableId']+'_filter input').wrap('<span></span>').after('<img src="'+options['searchCloseImage']+'" alt="" />');    
                        }

                        return oTable[options['htmlTableId']];

    });

};//$.fn.PCCSDataTable
})(jQuery);
