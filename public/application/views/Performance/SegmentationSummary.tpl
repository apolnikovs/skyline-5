{extends "Performance/MasterLayout.tpl"}

{block name=scripts append}
<script type="text/javascript">
  
    google.load('visualization', '1', { packages: ['corechart'] });

    var barchart;

    function drawVisualization() {
            // Create and populate the data table.
            var data = google.visualization.arrayToDataTable([
                    ['Service Centre', 'Response Time', 'Call Out Time', 'Inspection Time',
                            'Waiting For Repair', 'Customer Feedback', 'Completion Time',
                            'Final Delivery', 'Time To Claim'],
        {foreach $data as $row}
            ['{$row['ServiceCentre']|upper}', {$row['ResponseTime']|string_format:"%.2f"}, {$row['CallOutTime']|string_format:"%.2f"},
            {$row['InspectionTime']|string_format:"%.2f"}, {$row['WaitingForRepair']|string_format:"%.2f"},
        {$row['CustomerFeedback']|string_format:"%.2f"}, {$row['CompletionTime']|string_format:"%.2f"}, {$row['FinalDelivery']|string_format:"%.2f"}, {$row['TimeToClaim']|string_format:"%.2f"} ]{if not $row@last},{/if}
        {/foreach}
            ]);
            
            var options = { title: "Segmentation Graph",
                            height: data.getNumberOfRows() * 20,
                            chartArea: { width: 600, height: '100%', left: 200 },
                            fontSize: 10,
                            vAxis: { fontSize: 10 },
                            hAxis: { },
                            isStacked: true };
                    
            // Create and chart.
            barchart = new google.visualization.BarChart(document.getElementById('visualization'));
            
            // Must wait for the ready event in order to
            // request the chart and subscribe to events.
            google.visualization.events.addListener(barchart, 'ready', 
                             function() {
                             
                                // Every time the table fires the "select" event, it should call your
                                // selectHandler() function.
                                google.visualization.events.addListener(barchart, 'select',  onSelectHandler );
    
                             } );
             
            // draw the chart
            barchart.draw(data, options);

    }
       
    function onSelectHandler(e) {
        $.colorbox({ href:"{$_subdomain}/Performance/SegmentationDetail" });
    }

    google.setOnLoadCallback(drawVisualization);
</script>
{/block}

{block name=filters append}
{/block}

{block name=PerformanceBody}
<div id="visualization" class="span-23 last"></div>
{/block}