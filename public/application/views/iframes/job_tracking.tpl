{extends "iframes/MasterLayout.tpl"}


{block name=config}
{$Title = 'Skyline Job Tracking'}
{/block}
  
{block name=scripts}
<script>
/*
$.tools.validator.fn('[data-group]', 'Please complete at least one of these mandatory fields', function( input, value ) { 
            var group = input.attr('data-group');
            var is_valid = false;
            $('input[data-group='+group+']').each(
                    function() {
                        if (this.value !== '') is_valid = true;
                    }
                );
            return is_valid;
});*/

$.tools.validator.fn('[data-group]', 'Please complete at least two of these mandatory fields', function( input, value ) { 
            var group = input.attr('data-group');
            var is_valid = 0;
            $('input[data-group='+group+']').each(
                    function() {
                        if (this.value !== '') is_valid += 1;
                    }
                );
            return (is_valid >= 2);
});
        
$(document).ready(function() {  

        $("#trackingform").validator({
            message: '<div><em/></div>',
            position: 'top left',
            offset: [1, 40]
        });
        
        $("#trackingform2").validator({
            message: '<div><em/></div>',
            position: 'top left',
            offset: [1, 40]
        });
                     
});
</script>
{/block}

{block name=body}
    <div id="bookingFormHolder">
        
        <form id="trackingform" name="trackingform" action="{$_subdomain}/IFrame/JobTrackingForm" method="post">
            <fieldset class="iBookingSet">
            <div class="sectitle">Track a Job</div>
            <div class="iBookingSetLeft">
                <p class="info">
                Please enter the job number you were given at point of booking:
                </p>
            </div>
            <div class="iBookingItem">
                <label for="JobID" class="tLeft">{if $_skin eq ''}Swann {/if}Job Number:</label>
                <input type="text" name="JobID" id="JobID" required="required" />
            </div>
            <div class="iBookingItem">
                <label class="tLeft">&nbsp;</label>
                <input type="submit" name="submit" class="form-button" value="Submit" />
            </div>
            </fieldset>
        </form>
        <form id="trackingform2" name="trackingform2" action="{$_subdomain}/IFrame/JobTrackingForm" method="post">
            <p class="info">
                Alternatively, please enter at least 2 of the fields below with the information you supplied at point of booking:
            </p>
            <fieldset class="iBookingSet">
            <div class="iBookingItem">
                <label for="LastName" class="tLeft">Surname:</label>
                <input type="text" name="LastName" id="LastName" data-group="1" />
            </div>
            <div class="iBookingItem">
                <label for="Email" class="tLeft">Email Address:</label>
                <input type="email" name="Email" id="Email" data-group="1" />
            </div>
            <div class="iBookingItem">
                <label for="PostalCode" class="tLeft">Postcode:</label>
                <input type="text" name="PostalCode" id="PostalCode" data-group="1" />
            </div>
            <div class="iBookingItem">
                <label for="Phone" class="tLeft">Telephone Number: </label>
                <input type="text" name="Phone" id="Phone" data-group="1" />
            </div>
            <div class="iBookingItem">
                <label class="tLeft">&nbsp;</label>
                <input type="submit" name="submit2" class="form-button" value="Submit" />
            </div>
           <div class="clear"></div>
           </fieldset>
        </form>
    </div>

{/block}