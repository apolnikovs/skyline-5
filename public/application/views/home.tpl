{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Homepage"}
    {$PageId = $HomePage}
{/block}


{block name=scripts}


<script type="text/javascript" src="{$_subdomain}/js/jquery.autocomplete.js"></script> 
<script type="text/javascript" src="{$_subdomain}/js/product.form.validation.js"></script> 
<script type="text/javascript" src="{$_subdomain}/js/job.form.validation.js"></script> 
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<link rel="stylesheet" href="{$_subdomain}/css/tooltipster/tooltipster.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="{$_subdomain}/js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<style>
    table#status tbody tr:nth-child(even) td,  table#statustbody tr.even td {
    background: #fff;
        }
    table#status  {
        width: 920px;
        }
    table#status tbody tr td{
        width:120px;
        text-align:center;
        }
    </style>

<script type="text/javascript">

var ie = (function() {
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
    return v > 4 ? v : undef;
}());

    
$(document).ready(function() {


     $('#productSearch').keypress(function(e) {
            if(e.which == 13) {
                $(this).blur();
                if (validateProductForm()) $('#productSearchForm').submit();
                return false;
            }
        } );
        
        $('#jobSearch').keypress(function(e) {
            if(e.which == 13) {
                $(this).blur();
                if (validateJobForm()) $('#jobSearchForm').submit();
                return false;
            }
        } );  


    //$("#name").blur();
    
    {if $branches neq ''}
        
        $("#branch").hide();
    
    {/if}
    
    $UserBranchID = '';
    $ToBranchName = '';
    $FromBranchName = '';

    //blur handler for username
    $(document).on('blur', '#name', 
            function() {

                $uBDropDownList = '<option value="">{$page['Text']['select_branch']|escape:'html'}</option>';

                var $user_name = $("#name").val();

                if($user_name && $user_name!='')
                    {

                        $.post("{$_subdomain}/Data/getUserBranches/"+urlencode($user_name)+"/",        

                        '',      
                        function(data){
                        
                                var $userBranchesList = eval("(" + data + ")");
                                   

                                if($userBranchesList[0] && $userBranchesList[0]['Status']=="FAIL")
                                {
                                        $('#error').html("{$page['Errors']['username_notexists']}").show('slow');
                                        $("#branch").val("");
                                        $("#branch").hide();
                                }
                                else if($userBranchesList.length>0)
                                {
                                    $("#branch").show();
                                    
                                    for(var $i=0;$i<$userBranchesList.length;$i++)
                                    {
                                            
                                         if($userBranchesList[$i]['BranchID']==$userBranchesList[0]['UserBranchID'])
                                         {
                                             $selectedValue = ' selected="selected" ';    
                                             $UserBranchID = $userBranchesList[0]['UserBranchID'];
                                             $FromBranchName = $userBranchesList[$i]['BranchName'];
                                         }
                                         else
                                         {
                                             $selectedValue = '';
                                         }

                                        $uBDropDownList += '<option value="'+$userBranchesList[$i]['BranchID']+'"  '+$selectedValue+'    >'+$userBranchesList[$i]['BranchName']+'</option>';
                                    }
                                    
                                    $("#branch").html($uBDropDownList);
                                }
                                else
                                {
                                    $("#branch").val("");
                                    $("#branch").hide();
                                }

                                

                        });

                    }


            });  









    $('input').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    
    
    //$('form:first *:input[type!=hidden]:first').focus();
    {if $branch eq ''}
    $('#branch').focus();
    {else}
    $('#name').focus();
    {/if}
        
    
    
    
    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
        

        
        /* $('#branch').autocomplete( {
            lookup: BranchData
        } ); */
 
        /*$('#branch').autocomplete( {
            source: BranchData
        } ); */

     // $('ui-autocomplete').
     
        /*$('#branch').multiselect( {   
            noneSelectedText: "{$page['Text']['select_branch']|escape:'html'}"
        } );*/

	var $browerVHeight = 0;  
	if($.browser.version < 8) {
	    $browerVHeight = 6;
	}

	{*var $rightBox = parseInt($('#jobsInQueryFormDiv').height() + $('#overdue-jobs-form-div').height() + 51 + $browerVHeight);
	var $leftBox = parseInt($('#open-jobs-form-div').height());
     
	if($leftBox >= $rightBox) {
	    $('#overdue-jobs-form-div').height($('#open-jobs-form-div').height() - $('#jobsInQueryFormDiv').height() - 52 - $browerVHeight);
	} else {
	    $('#open-jobs-form-div').height($rightBox);
	}*}
        

    var ojheight = parseInt($('#open-jobs-form-div').css('height'));
    var jqheight = parseInt($('#jobsInQueryFormDiv').css('height'));
    var ovjheight = parseInt($('#overdue-jobs-form-div').css('height'));

    var rightbranchjobheigh1 = parseInt($('#jobsInQueryForm').css('height'));
    var rightbranchjobheigh2 = parseInt($('#overdue-jobs-form').css('height'));
    var leftbranchjobheigh = parseInt($('#open-jobs-form').css('height'));

    if(leftbranchjobheigh > (rightbranchjobheigh1 + rightbranchjobheigh2) ){ 
        var newheight = 5+(ovjheight+ (leftbranchjobheigh - (rightbranchjobheigh1 + rightbranchjobheigh2)))+"px";
        $('#overdue-jobs-form-div').css('height',newheight);
        
    }
    else{ 
        var newheight = 5+(ojheight + ((rightbranchjobheigh1 + rightbranchjobheigh2) - leftbranchjobheigh))+"px";
        $('#open-jobs-form-div').css('height',newheight);
        
        
    }

   
});

function validateForm() {
    
   
    if ($('#name').val()=='') {        
        $('#error').html("{$page['Errors']['username']}").show('slow');
        $('#name').focus();
        return false;
    }
    if ($('#password').val()=='') {
        $('#error').html("{$page['Errors']['password']}").show('slow');
        $('#password').focus();
        return false;
    }
    
    if ($('#branch').is(":visible") && $('#branch').val()=='') {        
        $('#error').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    
    
     if ($('#branch').is(":visible") && $('#branch').val()!=$UserBranchID) {        
        
        //$('#error').html("{$page['Errors']['branch_changed']}").show('slow');
        
        $ToBranchName  = $('#branch option:selected').text();
        $branch_changed_error = "{$page['Errors']['branch_changed']}";
        $branch_changed_error = $branch_changed_error.replace("%%from_branch_name%%", $FromBranchName); 
        $branch_changed_error = $branch_changed_error.replace("%%to_branch_name%%", $ToBranchName);
        
        return confirm($branch_changed_error);
        
        
        
        $('#branch').focus();
        
        return false;
    }
    
   // $('#name').trigger('blur');
    
    
    if ($('#error').is(":visible"))
    {
         return false;
    }
    
    
    return true;
}

function lostPassword() {
    if ($('#branch').val()=='') {        
        $('#error').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    if ($('#name').val()=='') {        
        $('#error').html("{$page['Errors']['username']}").show('slow');
        $('#name').focus();
        return false;
    }
    window.location = "{$_subdomain}/Login/lostPassword/" + $('#branch').val() + "/" + $('#name').val();
}




//======================================================================================================
//Do not put any code here which is not related to homepage Open/Overdue Jobs facility
//Anonymous namespace, isolating from global scope

(function() {
    
    
    var jobsTable;
    var globNode;
    var currentTable;
    var tableObj;
    
    
    $(document).ready(function() {
	
	if(getUrlVars()["jobsTable"]) {
	    $("#" + getUrlVars()["jobsTable"]).click();
	}
    
    });
    
    
    function jobsLoaded(id) {
	
	var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
			<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
		    </a>';
        $(html).insertAfter(".dataTables_filter input");
	
	$("#jobsDiv").slideDown(500);
        
	if(id == "appraisalRequired") {
	    $("#jobsLegend").text("Branch Open Jobs (Service Appraisal Required)");
	    $("#updateFieldset").hide();
	}
	if(id == "branchRepair") {
	    $("#jobsLegend").text("Branch Open Jobs (Branch Repair)");
	    $("#updateFieldset").hide();
	}
	if(id == "authRequired") {
	    $("#jobsLegend").text("Branch Open Jobs (Repair Authorisation Required)");
	    $("#updateFieldset").hide();
	}
        if(id == "awaitingParts") {
	    $("#jobsLegend").text("Branch Open Jobs (Awaiting Parts)");
	    $("#updateFieldset").show();
	}
	if(id == "inStore") {
	    $("#jobsLegend").text("Branch Open Jobs (Awaiting Service Provider Collection)");
	    $("#updateFieldset").show();
	}
	if(id == "withSupplier") {
	    $("#jobsLegend").text("Branch Open Jobs (Job In Progress With Service Provider)");
	    $("#updateFieldset").show();
	}
	if(id == "awaitingCollection") {
	    $("#jobsLegend").text("Branch Open Jobs (Service Complete Returned to Branch)");
	    $("#updateFieldset").show();
	}
	if(id == "customerNotified") {
	    $("#jobsLegend").text("Branch Open Jobs (Customer Notified)");
	    $("#updateFieldset").show();
	}
	
	if(id == "responseRequired") {
	    $("#jobsLegend").text("Jobs in Query (Response Required)");
	    $("#updateFieldset").hide();
	}
    
        if(id == "overdue1to5") {
	    $("#jobsLegend").text("Branch Overdue Jobs (1 to 5 days)");
	    $("#updateFieldset").hide();
	}
        if(id == "overdue6to10") {
	    $("#jobsLegend").text("Branch Overdue Jobs (6 to 10 days)");
	    $("#updateFieldset").hide();
	}
        if(id == "overdueOver10") {
	    $("#jobsLegend").text("Branch Overdue Jobs (over 10 days)");
	    $("#updateFieldset").hide();
	}

	$(".updateStatus:checked").removeAttr("checked");

	//init tooltip
	$(jobsTable.fnGetNodes()).find(".hoverCell").parent().css("padding","0");
	$(jobsTable.fnGetNodes()).find(".hoverCell").css("padding","7px 9px");
	$(jobsTable.fnGetNodes()).find(".hoverCell").tooltipster({
	    arrow: false,
	    delay: 0,		
	    followMouse: true,
	    position: 'top-right',
	    tooltipTheme: '.tableHover'
	});

	$(".tooltip").tooltipster({
	    arrow: false,
	    delay: 0,		
	    followMouse: true,
	    position: 'top-right',
	    tooltipTheme: '.tableHover'
	});

	if(ie !== 7) {
	    //Scrolls down to selected row, if no row selected to the table top, doesn't work in IE7
	    if($('.DTTT_selected') && $('.DTTT_selected').prop("tagName") == "TR") {
		$('html, body').animate({
		    scrollTop: $('.DTTT_selected').offset().top
		}, 1500);
	    } else {
		$('html, body').animate({
		    scrollTop: $('#' + currentTable + "Table").offset().top
		}, 1500);
	    }
	}
	
    }


    function redJobs(data) {
	var red = false;
	for(var i = 0; i < data.length; i++) {
	    if(data[i]["_aData"][10] == "1") {
		red = true;
	    }
	}
	if(red) {
	    html = "<div id='redLabel' style='margin-bottom:-27px; display:inline-block;'>\
			<span style='color:red;'>Red Jobs</span>\
			<span> = Warranty Authorisation Rejected</span></span>\
		    </div>\
	    ";
	    $(html).insertAfter("#jobsLegend");
	} else {
	    $("#redLabel").remove();
	}
    }


    function updateJobs(status) {

        $(".updateStatus[value=in_store]").attr("checked", false);
        $(".updateStatus[value=with_supplier]").attr("checked", false);
        $(".updateStatus[value=awaiting_parts]").attr("checked", false);
        $(".updateStatus[value=awaiting_collection]").attr("checked", false);
        $(".updateStatus[value=customer_notified]").attr("checked", false);
        $(".updateStatus[value=closed]").attr("checked", false);

	var result = [];
        var data = jobsTable.fnGetNodes();
        for(var i = 0; i < data.length; i++) {
            var el = $($(data[i]).find("td")[9]).find("input").attr("checked");
            if(el) {
                result.push(jobsTable.fnGetData(i)[6]);
            }
        }
        
        if(result.length == 0) {

            var html = '<div style="width:250px;">\
                            <p style="text-align:center">You must select at least one job.</p>\
                            <p style="text-align:center"><a href="#" class="blueBtn" id="closeModal">Close</a></p>\
                        </div>';
            $.colorbox({
                html:	    html, 
                scrolling:  false
            });
        
        } else {

            $.post("{$_subdomain}/Job/updateOpenJobsStatus", { jobs: JSON.stringify(result), status: status }, function(response) {

                var html = '<div style="width:250px;">\
                                <p style="text-align:center">Selected jobs have been updated.</p>\
                                <p style="text-align:center"><a href="#" class="blueBtn" id="closeModal">Close</a></p>\
                            </div>';

                $.colorbox({
                    html :	html, 
                    scrolling : false
                });

                $.post("{$_subdomain}/Job/refreshStatusNumbers", { }, function(response) {
                    var data = JSON.parse(response);
                    $("#appraisalRequired .opnJobsNum").text(data.appraisalRequired);
                    $("#branchRepair .opnJobsNum").text(data.branchRepair);
                    $("#authRequired .opnJobsNum").text(data.authRequired);
                    $("#awaitingParts .opnJobsNum").text(data.awaitingParts);                    
                    $("#inStore .opnJobsNum").text(data.inStore);
                    $("#withSupplier .opnJobsNum").text(data.withSupplier);
                    $("#awaitingCollection .opnJobsNum").text(data.awaitingCollection);
                    $("#customerNotified .opnJobsNum").text(data.customerNotified);
                    $(".opnJobsBar").each(function() {
                        var num = parseInt($(this).prev("span").text());
                        $(this).css("width", Math.round((num / data.total) * 100) + "px");
                    });
                });

            });

        }
	
    }


    function openSelectedJob() {
	//HTML5 compatible browsers only - adds to back button modified url which includes table id
	if(typeof window.history.pushState == 'function') { 
	    window.history.pushState('Object', 'Title', '{$_subdomain}/index/index?jobsTable=' + currentTable);
	}
	//var table = TableTools.fnGetInstance(currentTable + "Table");
	//var data = table.fnGetSelectedData();
	
	var jobs = getSelected();
	if(jobs) {
	    if(jobs.length == 1) {
		var job = jobsTable.fnGetData(jobs[0]);
		location.assign("{$_subdomain}/index/jobupdate/" + job[6] + "/?ref=" + currentTable);
	    } else {
		alert("You can only select one job.");
	    }
	} else {
	    alert("You have to select a job.");
	    return false;
	}
	
    }


    function selectStatus(node) {
	//Save current table state is manually called on row selection, because it doesn't happen by default
	globNode = parseInt(node[0]._DT_RowIndex);
	jobsTable.oApi._fnSaveState(jobsTable.fnSettings());
    }


    function rejectAuthRequest() {
    
	var html = "<fieldset>\
			<legend align='center'>Reject Authorisation Request</legend>\
			<p style='margin-top:10px;'>\
			    You have indicated that you want to reject the Authorisation request for the selected job. \
			    You must enter a reason why you have declined authorisation.\
			</p>\
			<textarea id='rejectText' style='width:97%; resize: none;'></textarea>\
			<p style='margin-top:15px; text-align:center;'>Please Note: If you proceed the selected job will be cancelled.</p>\
			<div style='text-align:center; margin-bottom:10px;'>\
			    <a class='btnStandard' id='rejectProceed' style='width:50px;'>Proceed</a>\
			    <a class='btnCancel' id='rejectCancel' style='width:50px;'>Cancel</a>\
			</div>\
		    </fieldset>\
		    ";
	
	$.colorbox({
	    html :	html, 
	    width:	"600px",
	    scrolling:	false
	});
    
    }
    
    
    function errorFn() {
	document.location.href = "{$_subdomain}/Login";
    }


    function goToSBPage() {
	var jobs = getSelected();
	if(jobs) {
	    if(jobs.length == 1) {
		var job = jobsTable.fnGetData(jobs[0])[6];
		location.assign("{$_subdomain}/index/jobupdate/" + job[6] + "/?ref=" + currentTable);
	    } else {
		alert("You can only select one job.");
		return false;
	    }
	} else {
	    alert("You have to select a job.");
	    return false;
	}
	location.assign("{$_subdomain}/index/jobupdate/" + job + "/?ref=" + currentTable + "&appraisal=1");
    }


    function openPrintModal() {
    
	var table = TableTools.fnGetInstance(currentTable + "Table");
	var data = table.fnGetSelectedData();
	
	var jobs = getSelected();
	if(jobs) {
	    if(jobs.length == 1) {
		var job = jobsTable.fnGetData(jobs[0])[6];
	    } else {
		alert("You can only select one job.");
		return false;
	    }
	} else {
	    alert("You have to select a job.");
	    return false;
	}
	
	var html = "<fieldset>\
			<legend align='center'>Print Options</legend>\
			<br/>\
			<input name='printRadio' type='radio' value ='2' /> Print Customer Receipt\
			<br/>\
			<input name='printRadio' type='radio' value ='1' /> Print Job Card\
                        <br/>\
			<input name='printRadio' type='radio' value ='3' /> Print Service Report\
			<br/>\
			<br/>\
			<div style='width:100%; text-align:center;'>\
			    <a href='#' id='print' style='width:50px;' class='btnStandard'>Print</a>&nbsp;\
			    <a href='#' id='cancel' style='width:50px;' class='btnCancel'>Cancel</a>\
			</div>\
		    </fieldset>\
		   ";

	$.colorbox({
	    html :	html, 
	    width:	"320px",
	    scrolling:	false
	});

	$(document).on("click", "#print", function() {
	    var printType = $("input[name=printRadio]:radio:checked").val();
	    $.colorbox.close();
            var url = "{$_subdomain}/Job/printJobRecord/" + job + "/" + printType + "/?pdf=1";
	    $("#print").die();
            window.open(url, "_blank");
            window.focus();
            return false;
	});
	
    }


    function getSelected() {
	var selected = [];
	var rows = jobsTable.fnGetNodes();
	for (var i = 0; i < rows.length; i++) {
	    if($($(rows[i]).find("td").toArray()[9]).find("input").is(":checked")) {
		selected.push(rows[i]);
	    }
	}
	if (selected.length == 0) {
	    var table = TableTools.fnGetInstance(currentTable + "Table");
	    var highlighted = table.fnGetSelected();
	    if (highlighted.length == 0) {
		return false;
	    } else {
		return [highlighted[0]];
	    }
	} else {
	    return selected;
	}
    }


    $(document).on("click", "#rejectCancel", function() {
	$.colorbox.close();
	return false;
    });


    $(document).on("click", "#rejectProceed", function() {
	
	var text = $("#rejectText").val();
	
	if(text == "") {
	    
	    alert("You must give a reason before you proceed.");
	    
	} else {

	    var table = TableTools.fnGetInstance(currentTable + "Table");
	    var jobID = table.fnGetSelectedData()[0][6];

	    $.post("{$_subdomain}/Job/authRejected", { jobID: jobID, reasonText: text }, function(response) {
		
		jobsTable.fnReloadAjax(null, null, null, true);
		
		$.post("{$_subdomain}/Job/refreshStatusNumbers", { }, function(response) {
		    var data = JSON.parse(response);
                    $("#appraisalRequired .opnJobsNum").text(data.appraisalRequired);
                    $("#branchRepair .opnJobsNum").text(data.branchRepair);
		    $("#authRequired .opnJobsNum").text(data.authRequired);
                    $("#awaitingParts .opnJobsNum").text(data.awaitingParts);  
		    $("#inStore .opnJobsNum").text(data.inStore);
		    $("#withSupplier .opnJobsNum").text(data.withSupplier);
		    $("#awaitingCollection .opnJobsNum").text(data.awaitingCollection);
		    $("#customerNotified .opnJobsNum").text(data.customerNotified);
		    $(".opnJobsBar").each(function() {
			var num = parseInt($(this).prev("span").text());
			$(this).css("width", Math.round((num / data.total) * 100) + "px");
		    });
		});
		
		$.colorbox.close();
		
	    });
    
	}
	
	return false;
    });


    $(document).on("click", "#delSearch", function() {
	$(".dataTables_filter input").val("");
	jobsTable.fnFilter("");
	return false;
    });
    

    $(document).on("click", "#updateStatusBtn", function() {

	var status = $(".updateStatus:checked").val();
        
	if(status) {
	
	    updateJobs(status);
	    /*
	    if(status == "with_supplier") {
		
		var html = "<fieldset>\
				<legend align='center'>Print Option</legend>\
				<p style='margin:10px 0px;'>Do you want to print a Despatch Manifest?</p>\
				<div style='width:100%; text-align:center;'>\
				    <a href='#' id='printManifest' style='width:30px;' class='DTTT_button'>Yes</a>&nbsp;\
				    <a href='#' id='cancel' style='width:30px;' class='DTTT_button'>No</a>\
				</div>\
			    </fieldset>\
			   ";
    
		$.colorbox({
		    html :	html, 
		    width:	"400px",
		    scrolling:	false
		});
		
		$(document).on("click", "#printManifest", function() {
		    var result = [];
		    var data = jobsTable.fnGetNodes();
		    for(var i = 0; i < data.length; i++) {
			var el = $($(data[i]).find("td")[9]).find("input").attr("checked");
			if(el) {
			    result.push(jobsTable.fnGetData(i)[6]);
			}
		    }
		    return false;
		    document.location.href = "{$_subdomain}/Job/printDespatchManifest/?data=" + JSON.stringify(result);
		});

	    }
	    */
        }
	return false;
    });


    $(document).on("click", "#closeModal", function() {
	$.colorbox.close();
	jobsTable.fnReloadAjax(null,null,null,true);
	return false;
    });


    $(document).on("dblclick", "tr", function() {
	//HTML5 compatible browsers only - adds to back button modified url which includes table id
	if(typeof window.history.pushState == 'function') { 
	    window.history.pushState('Object', 'Title', '{$_subdomain}/index/index?jobsTable=' + currentTable);
	}
	location.assign("{$_subdomain}/index/jobupdate/" + jobsTable.fnGetData(this)[6] + "/?ref=" + currentTable);
    });

    
    {if isset($loggedin_user->Permissions["AP7036"]) || $loggedin_user->UserType == "Admin"}
    
	$(document).on("click", "#authRequiredTable tr > td:first-child+td+td+td+td", function() {
	    $(this).css("padding","0");
	    var html = "<input type='text' id='authInput' style='width:80%; height:13px; margin-left:4px; margin-top:3px;' />";
	    $(this).html(html);
	    $("#authInput").focus();
	    return false;
	});


	$(document).on("click", "#authInput", function() {
	    return false;
	});


	$(document).on("blur", "#authInput", function() {
	    if($(this).val() != "") {
		var table = TableTools.fnGetInstance(currentTable + "Table");
		var data = table.fnGetSelectedData();
		$.post("{$_subdomain}/Job/setAuthNo", { jobID: data[0][6], authNo: $("#authInput").val() }, function(result) {
		    jobsTable.fnReloadAjax(null,null,null,true);
		    $.post("{$_subdomain}/Job/refreshStatusNumbers", { }, function(response) {
			var data = JSON.parse(response);
			$("#appraisalRequired .opnJobsNum").text(data.appraisalRequired);
			$("#branchRepair .opnJobsNum").text(data.branchRepair);
			$("#authRequired .opnJobsNum").text(data.authRequired);
                        $("#awaitingParts .opnJobsNum").text(data.awaitingParts);
			$("#inStore .opnJobsNum").text(data.inStore);
			$("#withSupplier .opnJobsNum").text(data.withSupplier);
			$("#awaitingCollection .opnJobsNum").text(data.awaitingCollection);
			$("#customerNotified .opnJobsNum").text(data.customerNotified);
		    });
		});
	    } else {
		$(this).remove();
	    }
	    return false;
	});


	$(document).on("keypress", "#authInput", function(e) {
	    if(e.which == 13) {
		if(this && $(this).val() != "") {
		    var table = TableTools.fnGetInstance(currentTable + "Table");
		    var data = table.fnGetSelectedData();
		    $.post("{$_subdomain}/Job/setAuthNo", { jobID: data[0][6], authNo: $("#authInput").val() }, function(result) {
			jobsTable.fnReloadAjax(null,null,null,true);
			$.post("{$_subdomain}/Job/refreshStatusNumbers", { }, function(response) {
			    var data = JSON.parse(response);
			    $("#appraisalRequired .opnJobsNum").text(data.appraisalRequired);
			    $("#branchRepair .opnJobsNum").text(data.branchRepair);
			    $("#authRequired .opnJobsNum").text(data.authRequired);
                            $("#awaitingParts .opnJobsNum").text(data.awaitingParts);
			    $("#inStore .opnJobsNum").text(data.inStore);
			    $("#withSupplier .opnJobsNum").text(data.withSupplier);
			    $("#awaitingCollection .opnJobsNum").text(data.awaitingCollection);
			    $("#customerNotified .opnJobsNum").text(data.customerNotified);
			});
		    });
		} else {
		    $(this).remove();
		}
		return false;
	    }
	});

    {/if}


    var btns = [
	"#appraisalRequired",
	"#branchRepair",
	"#authRequired",
        "#awaitingParts",
	"#inStore", 
	"#withSupplier", 
	"#awaitingCollection", 
	"#customerNotified", 
	"#overdue1to5", 
	"#overdue6to10", 
	"#overdueOver10",
	"#responseRequired"
    ];


    $(document).on("click", btns.join(), function() {

	document.body.style.cursor = "wait";
	
        $(".dataTables_wrapper, #redLabel").remove();

        var the = this;

	var status;
        jobsTable = null;
        globNode = null;
        currentTable = null;

        var id = $(this).attr("id");

	currentTable = id;

        var html = '<table id="tempTable" class="browse" style="width:100%;">\
                        <thead>\
                            <tr>\
                                <th>Date</th>\
                                <th class="tooltip" title="Number of days between the booking date and today">Days</th>\
                                <th>Timeline</th>\
                                <th>Customer</th>\
                                <th>Phone</th>\
                                <th>Product Type</th>\
                                <th>SL No</th>\
                                <th>Service Provider</th>\
                                <th>Job No</th>\
				<th>Select</th>\
                            </tr>\
                        </thead>\
                        <tbody></tbody>\
                    </table>';
        
	$(html).insertAfter("#jobsLegend");

        if(id == "appraisalRequired") {
            status = "appraisal_required";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "branchRepair") {
            status = "branch_repair";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "authRequired") {
            status = "auth_required";
	    $($("#tempTable thead tr").find("th")[4]).text("Auth No");
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "awaitingParts") {
            status = "awaiting_parts";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "inStore") {
            status = "in_store";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "withSupplier") {
            status = "with_supplier";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "awaitingCollection") {
            status = "awaiting_collection";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "customerNotified") {
            status = "customer_notified";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        
        if(id == "responseRequired") {
            status = "query";
            $("#tempTable").attr("id", currentTable + "Table");
        }
    
        if(id == "overdue1to5" || id == "overdue6to10" || id == "overdueOver10") {
	    $($("#tempTable").find("th").toArray()[2]).text("Overdue");
	    $($("#tempTable").find("th").toArray()[2]).attr("class", "tooltip");
	    $($("#tempTable").find("th").toArray()[2]).attr("title", "Number of days the expected service interval is overdue");
	}	
    
        if(id == "overdue1to5") {
            status = "1to5";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "overdue6to10") {
            status = "6to10";
            $("#tempTable").attr("id", currentTable + "Table");
        }
        if(id == "overdueOver10") {
            status = "over10";
            $("#tempTable").attr("id", currentTable + "Table");
        }

        tableObj = {
            bAutoWidth:	false,
            aoColumns: [
                { sWidth: '6%' },
                { sWidth: '6%', sType: "num-html" },
                { sWidth: '9%' }, 
                { sWidth: '16%' }, 
                { sWidth: '11%', sDefaultContent: "" }, 
                { sWidth: '18%' },
                { sWidth: '8%' },
                { sWidth: '14%' },
                { sWidth: '8%' },
                { sWidth: '4%', bSortable:false }
            ],
            bDestroy:           true,
            bStateSave:         true,
            bServerSide:        false,
            bProcessing:        false,
            htmlTableId:        'jobsTable',
            sDom:               "ft<'#dataTables_child'>Trpli",
            sPaginationType:    "full_numbers",
            bPaginate:          true,
            bSearch:            false,
            iDisplayLength:     10,
            sAjaxSource:        '{$_subdomain}/Job/getJobs?status=' + status,
	    fnServerData: function(sSource, aoData, fnCallback, oSettings) {
		oSettings.jqXHR = $.ajax({
		    dataType:	"json",
		    type:	"POST",
		    url:	sSource,
		    data:	aoData,
		    success:	fnCallback,
		    error:	errorFn
		});
	    },
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
	    },
            oTableTools: {
                sRowSelect: "single",
                aButtons: [
                    {
                        sExtends:	"text",
                        sButtonText:    "View Job Details",
                        fnClick: function() { openSelectedJob(); }
                    }
                ],
                fnRowSelected: function(node) { selectStatus(node); }
            },
            fnInitComplete: function(oSettings) { 
		//Reselect row if selected last time
		if((globNode && globNode != "") || globNode === 0) {
		    var node = this.fnGetNodes(globNode);
		    var select = TableTools.fnGetInstance(oSettings.sTableId);
		    $(node).addClass(select.selectedClass);
		    select.fnSelect(node); 
		}
                jobsLoaded($(the).attr("id"));
		document.body.style.cursor = "default";
	    },
            fnStateSaveParams: function(oSettings, oData) {
                oData.currentNode = globNode;
            },
            fnStateLoadParams: function(oSettings,oData) {
                globNode = oData.currentNode;
            },

	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		if(aData[10] == 1) {
		    $(nRow).css("color", "red");
		    $($($(nRow).find("td"))[7]).text("Authorisation Rejected");
		}
	    },	
                
	    fnDrawCallback: function(oSettings) {
		redJobs(oSettings.aoData);
	    },
		
            //Save current table state to the database, overriding default save to cookie method
            fnStateSave: function(oSettings, oData) {
		if(jobsTable && currentTable && oData) {
                    $.ajax({
                        url:        "{$_subdomain}/index/saveTableState?tableID=" + currentTable,
                        data:       { data : JSON.stringify(oData) },
			async:	    true,
                        dataType:   "text",
                        type:       "POST",
                        success:    function(result) { },
			error:	    function(jqXHR, textStatus, errorThrown) { }
                    });
                }
            },
            
            //Load current table state from the database, overriding default load from cookie method
            fnStateLoad: function(oSettings) {
                var o;
                $.ajax({
                    url:	"{$_subdomain}/index/loadTableState?tableID=" + currentTable,
                    async:	false,
                    dataType:	"json",
                    type:	"POST",
                    success: function(json) {
                        o = JSON.parse(json);
                    }
                });
		if(o) {
		    if(oSettings._iDisplayLength >= o.currentNode) {
			o.currentNode = null;
		    }
		    return o;
		}
            }

        }
    
	/*
        if(id == "inStore" || id == "withSupplier" || id == "awaitingCollection" || id == "customerNotified") {
	    tableObj.aoColumns = [
                { sWidth: '6%' },
                { sWidth: '6%', sType: "num-html" },
                { sWidth: '9%' }, 
                { sWidth: '17%' }, 
                { sWidth: '10%', sDefaultContent: "" }, 
                { sWidth: '16%' },
                { sWidth: '10%' },
                { sWidth: '14%' },
                { sWidth: '8%' },
                { sWidth: '4%', bSortable:false }
            ];
            $("#" + currentTable + "Table tr").append("<th>Select</th>");
	}
	*/
        // Add Service Appraisal Button
        var serviceAppraisalBtn = {
            sExtends:	    "text",
	    sButtonText:    "Service Appraisal",
	    sButtonClass:   "width200",
	    fnClick: function() { goToSBPage(); return false; },
	    fnInit: function(nButton, oConfig) {
		$(nButton).attr("id", "servAppBtn");
		if(currentTable == "authRequired" || currentTable == "appraisalRequired")
		    $(nButton).css("display", "none");
	    }
        };
        tableObj.oTableTools.aButtons.push(serviceAppraisalBtn);
	//Add RA Rejected facility
	{if isset($loggedin_user->Permissions["AP7036"]) || $loggedin_user->UserType == "Admin"}
	    if(id == "authRequired") {
		var btn = { 
		    sExtends:	    "text",
		    sButtonText:    "Reject Authorisation",
		    sButtonClass:   "width200",
		    fnClick: function() { rejectAuthRequest(); return false; },
                    fnInit: function(nButton, oConfig) { $(nButton).css("margin", "0px 00px 0px 10px"); }
		};
		tableObj.oTableTools.aButtons.push(btn);
	    }
	{/if}

	//Add Service Appraisal Button
	{if isset($loggedin_user->Permissions["AP12002"]) || $loggedin_user->UserType == "Admin"}
	    if(id == "appraisalRequired") {
		var btn = { 
		    sExtends:	    "text",
		    sButtonText:    "Service Appraisal",
		    sButtonClass:   "width200",
		    fnClick:	    function() { goToSBPage(); return false; }
		};
		tableObj.oTableTools.aButtons.push(btn);
	    }
	{/if}
	
	{if isset($loggedin_user->Permissions["AP12004"]) || $loggedin_user->UserType == "Admin"}
	    if(id == "branchRepair") {
		var btn = { 
		    sExtends:	    "text",
		    sButtonText:    "Branch Repair",
		    sButtonClass:   "width200",
		    fnClick:	    function() { goToSBPage(); return false; },
                    fnInit: function(nButton, oConfig) { $(nButton).css("margin", "10px 10px 0px 0px"); }
		};
		tableObj.oTableTools.aButtons.push(btn);
	    }
	{/if}
	//Add Print Routines button
	var btn = { 
	    sExtends:	    "text",
	    sButtonText:    "Print Routines",
	    sButtonClass:   "width200",
	    fnClick: function() { 
		openPrintModal();
		return false; 
	    },
	    fnInit: function(nButton, oConfig) {
		$(nButton).attr("id", "printBtn");
		if(currentTable == "authRequired" || currentTable == "branchRepair")
		    $(nButton).css("margin", "10px 10px 0px 0px");
                 else if(currentTable == "awaitingParts")
                    $(nButton).css("margin", "10px 10px 0px 0px");
                else if(currentTable == "inStore")
                    $(nButton).css("margin", "0px 10px 0px 0px");
                else if(currentTable == "withSupplier" || currentTable == "awaitingCollection" || currentTable == "customerNotified")
                    $(nButton).css("margin", "10px 10px 0px 0px");
                else if(currentTable == "appraisalRequired")
                    $(nButton).css("margin", "10px 0px 0px 0px");
	    }
	};
	tableObj.oTableTools.aButtons.push(btn);

	//Add Rapid Processes button
	if(currentTable == "inStore") {
	    var btn = {
		sExtends:	"text",
		sButtonText:    "Rapid Process",
		sButtonClass:   "width200",
		fnClick: function() {
		    $.post("{$_subdomain}/index/rapidProcessModal", { }, function(response) {
			$.colorbox({
			    html :	    response, 
			    width:	    "800px",
			    scrolling:	    false,
			    overlayClose:   false,
			    onComplete: function() { initRapidTable(); },
			    onClosed: function() { 
				$("#next").die();
				$("input[name=despatchType]").die();
				$(".delRow").die();
			    }
			});
		    });
		    return false; 
		},
		fnInit: function(nButton, oConfig) {
		    $(nButton).attr("id", "rapidBtn");
		    $(nButton).css("margin", "10px 0px 0px 0px");
		}
	    };
	    tableObj.oTableTools.aButtons.push(btn);
	}
	
        jobsTable = $("#" + currentTable + "Table").dataTable(tableObj);
	
	if(currentTable == "awaitingParts" || currentTable == "inStore" || currentTable == "authRequired" || currentTable == "appraisalRequired" || currentTable == "branchRepair") {
	    $(".dataTables_info").css("margin-top", "-30px");
	}
	
	return false;

    });
    
    
    var rapidTable;
    var jobIDs;
    var changed = false;
    
    function initRapidTable() {
    
	$("#despatchDate").datepicker({ dateFormat: "yy-mm-dd" });
    
	jobIDs = [];
	var jobs = jobsTable.fnGetData();
	for(var i = 0; i < jobs.length; i++) {
	    jobIDs.push(parseInt(jobs[i][6]));
	}
	
	rapidTable = $("#rapidTable").dataTable({
            aoColumns: [
		{ bSortable: false },
		{ bSortable: false },
		{ bSortable: false }
            ],
	    aaData:	    [],
	    bSort:	    false,
            bAutoWidth:	    false,
            bDestroy:       true,
            bStateSave:     true,
            bServerSide:    false,
            bProcessing:    true,
            htmlTableId:    "rapidTable",
            sDom:           "t<'#dataTables_child'>Trpl",
            bPaginate:      false,
            bSearch:        false,
            iDisplayLength: 1000,
            oTableTools: {
		aButtons: false,
                sRowSelect: "single",
                fnRowSelected: function(node) { }
            },
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		var cell = $(nRow).find("td").toArray()[2];
		var html = "<a href='#' class='delRow'><img src='{$_subdomain}/css/Base/images/red_cross.png' /></a>";
		$(cell).html(html);
	    }
	});


	$(document).on("change", "input[name=despatchType]", function () {
	
	    change = true;
	
	    if($("input[name=despatchType]").is(":checked") && $("input[name=despatchType]:checked").val() == "selected") {

		if($("#consignmentNo").val() == "") {
		    modal("Please enter a Consignment Number before selecting the Despatch Type.");
		    change = true;
		    $("input[name=despatchType]").attr("checked", false);
		    return false;
		}
    
		$("#nextWrap, #jobNo, label[for=jobNo]").hide();
		$("input[name=despatchType]").attr("disabled", true);
		$("#consignmentNo").css("margin-bottom", "10px");
		$.colorbox.resize();
		
		var data = jobsTable.fnGetData();
		for(var i = 0; i < data.length; i++) {
		    var row = jobsTable.fnGetNodes(i);
		    if($($(row).find("td").toArray()[9]).find("input").is(":checked")) {
			rapidTable.fnAddData([data[i][6], $("#consignmentNo").val(), null]);
		    }
		}

		$("#total").text(rapidTable.fnGetData().length);
		
		$.colorbox.resize();

		if(rapidTable.fnGetData().length == 0) {
		    modal("You must select at least one record from Awaiting Service Provider Collection table to use this option");
		    return false;
		}
	    }	    
	});


	$(document).on("click", "#finish, #printRapid", function() {
	    //if($(this).attr("id") == "printRapid") return false;
	    if(rapidTable.fnGetData().length == 0) {
		modal("You must have at least one job selected to proceed.");
		return false;
	    }
	    if($(this).attr("id") == "printRapid") {
		confirm("Are you sure you want to proceed?", true);
	    } else {
		confirm("Are you sure you want to proceed?", false);
	    }
	    return false;
	});
	
	
	function finish(print) {
	    document.body.style.cursor = "wait";
	    var jobs = JSON.stringify(rapidTable.fnGetData());
	    //$("#modP").dialog("destroy");
	    //$.colorbox.close();
	    $.post("{$_subdomain}/Job/despatchJobs", { jobs: jobs, consignmentNo: $("#consignmentNo").val(), consignmentDate: $("#despatchDate").val() }, function() {
		jobsTable.fnReloadAjax(null, null, null, true);
		$.post("{$_subdomain}/Job/refreshStatusNumbers", { }, function(response) {
		    var data = JSON.parse(response);
		    $("#appraisalRequired .opnJobsNum").text(data.appraisalRequired);
		    $("#authRequired .opnJobsNum").text(data.authRequired);
                    $("#awaitingParts .opnJobsNum").text(data.awaitingParts);
		    $("#inStore .opnJobsNum").text(data.inStore);
		    $("#withSupplier .opnJobsNum").text(data.withSupplier);
		    $("#awaitingCollection .opnJobsNum").text(data.awaitingCollection);
		    $("#customerNotified .opnJobsNum").text(data.customerNotified);
		    
		    if(print) {
		    
			var html = "<fieldset>\
					<legend align='center'>Print Options</legend>\
					<br/>\
					<input name='printRadio' type='radio' value ='2' /> Customer Receipt\
					<br/>\
					<input name='printRadio' type='radio' value ='1' /> Job Card\
					<br/>\
					<input name='printRadio' type='radio' value ='3' /> Service Report\
					<br/>\
					<input name='printRadio' type='radio' value ='4' disabled='disabled' /> Despatch Manifest\
					<br/>\
					<br/>\
					<div style='width:100%; text-align:center;'>\
					    <a href='#' id='printRapidConfirm' style='width:50px;' class='btnConfirm'>Print</a>&nbsp;\
					    <a href='#' id='cancel' style='width:50px;' class='btnCancel'>Cancel</a>\
					</div>\
					<br/>\
				    </fieldset>\
				   ";

			$.colorbox({
			    html :	html, 
			    width:	"320px",
			    scrolling:	false,
			    overlayClose: false,
			    onComplete: function() {
				document.body.style.cursor = "default";

				$(document).on("click", "#printRapidConfirm", function() {
				    var printType = $("input[name=printRadio]:radio:checked").val();
				    $.colorbox.close();
				    var url = "{$_subdomain}/Job/printJobRecord//" + printType + "/?pdf=1&jobs=" + jobs;
				    $("#print").die();
				    window.open(url, "_blank");
				    window.focus();
				    return false;
				});

			    },
			    onClosed: function() {
				$("#printRapidConfirm").die();
			    }
			});
			
		    } else {
		    
			document.body.style.cursor = "default";
			
		    }
		});
	    });
	}


	$(document).on("change", "#jobNo", function() {
	    change = true;
	});


	$(document).on("click", "#next", function() {
	    if(change) {
		nextJob();
	    }
	    return false;
	});
	
	
	$(document).on("blur", "#jobNo", function() {
	    if(change) {
		nextJob();
	    }
	    return false;
	});
	
	
	function nextJob() {

	    change = false;

	    if(!$("input[name=despatchType]").is(":checked")) {
		modal("You must select Despatch Type first.");
		change = true;
		return false;
	    }
	
	    if($("#consignmentNo").val() == "") {
		modal("You must enter Consignment No first.");
		change = true;
		return false;
	    }
	
	    var jobNo = parseInt($("#jobNo").val());
	    if($.inArray(jobNo, jobIDs) != -1) {
		var data = rapidTable.fnGetData();
		for(var i = 0; i < data.length; i++) {
		    if(parseInt(data[i][0]) === jobNo) {
			modal("This job is already added.");
			return false;
		    }
		}
		rapidTable.fnAddData([jobNo, $("#consignmentNo").val(), null]);

		$("input[name=despatchType]").attr("disabled", true);

		if($("input[name=despatchType]:checked").val() == "bulk") {
		    $("#jobNo").val("").focus();
		} else if($("input[name=despatchType]:checked").val() == "individual") {
		    $("#jobNo").val("");
		    $("#consignmentNo").val("").focus();
		}

		$("#total").text(rapidTable.fnGetData().length);
		$.colorbox.resize();
	    } else {
		modal("This job does not exist in Awaiting Service Provider Collection table.");
		return false;
	    }
	    
	    return false;
	}
	
	
	$(document).on("click", ".delRow", function() {
	    console.log($(this).parent().parent().toArray()[0]);
	    rapidTable.fnDeleteRow($(this).parent().parent().toArray()[0]);
	    $("#total").text(rapidTable.fnGetData().length);
	    $.colorbox.resize();
	    return false;
	});
	
	
	function modal(text) {
	    $("#modP").remove();
	    var html = "<p id='modP'>" + text + "</p>";
	    $(html).appendTo("body");
	    $("#modP").dialog({
		resizable:  false,
		draggable:  false,
		height:	    "auto",
		modal:	    true,
		title:	    "Alert",
		buttons: {
		    Close: {
			click:	function() {
				    $(this).dialog("destroy");
				},
			class:	"btnCancel",
			text:	"Close"
		    }
		}
	    });	
	}
	
	
	function confirm(text, print) {
	    $("#modP").remove();
	    var html = "<p id='modP'>" + text + "</p>";
	    $(html).appendTo("body");
	    $("#modP").dialog({
		resizable:  false,
		draggable:  false,
		height:	    "auto",
		modal:	    true,
		title:	    "Confirm",
		buttons: {
		    Yes: {
			click:	function() {
				    finish(print);
				    $(this).dialog("destroy");
				},
			class:	"btnConfirm marginRight10",
			text:	"Yes"
		    },
		    No: {
			click:	function() {
				    $(this).dialog("destroy");
				},
			class:	"btnCancel",
			text:	"No"
		    }
		}
	    });
	}
	
	
    }


})();

//End of isolating scope


</script>


{/block}


{block name=body}

    
{if $session_user_id}
    
<div class="breadcrumb" >
    <div>
        Home Page
    </div>
</div>
    
{include file='include/menu.tpl'}

{else}
   
    
       <h3>Welcome to Skyline Login & Catalogue Search Page</h3>
      
       <label>Search for a catalogue to display specific service instructions and the nominated service partner's details.</label>
      
{/if}


                                    
<div class="main" id="home">
    
    {if $ProductDatabaseEnabled}
        {if isset($enableCatalogueSearch)}
	    {if $enableCatalogueSearch}
		<div class="SearchPanel">
		    {include file='include/product_search.tpl'}
		</div>
	    {/if}
	{/if}
    {/if}
    
    
    {if $AP7037}
	<div class="SearchPanel">
	    {include file='include/policy_search_panel.tpl'}
	</div> 
    {/if}
    
    
    {if ($ShowJobBookingPanel || ($productNoResultsFlag eq false && $session_user_id)) && $AP11000}
        <div class="SearchPanel">
            {include file='include/job_booking_panel.tpl'}
        </div> 
    {/if}
    
    
    {if $session_user_id}
        
        {if $AP7003}
            
	<div class="SearchPanel">
            {include file='include/job_search.tpl'}
        </div> 
        
        {/if}
                
        {if $displayOpenOverdueJobs}
            
                    <form id="open-jobs-form" class="inline span-12" style="padding-bottom: 10px;">
		<fieldset>
		    <legend>Branch Open Jobs</legend>
                    
                    <div id="open-jobs-form-div" >
		    
		    {if isset($jobsAppraisalRequiredNumber)}
			<p {if $jobsAppraisalRequiredNumber == 0}style="display:none;"{/if}>
			    <label>Service Appraisal Required</label>
			    <a id="appraisalRequired" href="#">
				<span class="opnJobsNum">{$jobsAppraisalRequiredNumber}</span>
				<span class="job-block opnJobsBar" id="appraisalRequiredBar" style="width: {if $jobsAppraisalRequiredNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsAppraisalRequiredNumber/$openJobsTotal) * 100)}{/if}px; background-color: #feff00;">&nbsp;</span>
			    </a>
			</p>
		    {/if}
		    
		    {if isset($jobsBranchRepairNumber)}
			<p {if $jobsBranchRepairNumber == 0}style="display:none;"{/if}>
			    <label>Branch Repair</label>
			    <a id="branchRepair" href="#">
				<span class="opnJobsNum">{$jobsBranchRepairNumber}</span>
				<span class="job-block opnJobsBar" id="branchRepairBar" style="width: {if $jobsBranchRepairNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsBranchRepairNumber/$openJobsTotal) * 100)}{/if}px; background-color: #feff00;">&nbsp;</span>
			    </a>
			</p>
		    {/if}
		    
		    <p {if $jobsAuthRequiredNumber == 0}style="display:none;"{/if}>
			<label>Repair Authorisation Required</label>
			<a id="authRequired" href="#">
			    <span class="opnJobsNum">{$jobsAuthRequiredNumber}</span>
			    <span class="job-block opnJobsBar" id="authRequiredBar" style="width: {if $jobsAuthRequiredNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsAuthRequiredNumber/$openJobsTotal) * 100)}{/if}px; background-color: #DBEEF4;">&nbsp;</span>
			</a>
		    </p>
                    
                    <p>
			<label> Awaiting Parts </label>
			<a id="awaitingParts" href="#">
			    <span class="opnJobsNum">{$jobsAwaitingPartsNumber}</span>
			    <span class="job-block opnJobsBar" id="awaitingPartsBar" style="width: {if $jobsAwaitingPartsNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsAwaitingPartsNumber/$openJobsTotal) * 100)}{/if}px; background-color: #C3D69B;">&nbsp;</span>
			</a>
		    </p>
		    <p>
			<label>Awaiting Service Provider Collection</label>
			<a id="inStore" href="#">
			    <span class="opnJobsNum">{$jobsInStoreNumber}</span>
			    <span class="job-block opnJobsBar" id="inStoreBar" style="width: {if $jobsInStoreNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsInStoreNumber/$openJobsTotal) * 100)}{/if}px; background-color: #C3D69B;">&nbsp;</span>
			</a>
		    </p>
		    <p>
			<label>Job In Progress With Service Provider</label>
			<a id="withSupplier" href="#">
			    <span class="opnJobsNum">{$jobsWithSupplierNumber}</span>
			    <span class="job-block opnJobsBar" id="withSupplierBar" style="width: {if $jobsWithSupplierNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsWithSupplierNumber/$openJobsTotal) * 100)}{/if}px; background-color: #D99694;">&nbsp;</span>
			</a>
		    </p>
		    <p>
			<label>Service Complete Returned to Branch</label>
			<a id="awaitingCollection" href="#">
			    <span class="opnJobsNum">{$jobsAwaitingCollectionNumber}</span>
			    <span class="job-block opnJobsBar" id="awaitingCollectionBar" style="width: {if $jobsAwaitingCollectionNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsAwaitingCollectionNumber/$openJobsTotal) * 100)}{/if}px; background-color: #558ED5;">&nbsp;</span>
			</a>
		    </p>
		    <p>
			<label>Customer Notified</label>
			<a id="customerNotified" href="#">
			    <span class="opnJobsNum">{$jobsCustomerNotifiedNumber}</span>
			    <span class="job-block opnJobsBar" id="customerNotifiedBar" style="width: {if $jobsCustomerNotifiedNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsCustomerNotifiedNumber/$openJobsTotal) * 100)}{/if}px; background-color: #00B050;">&nbsp;</span>
			</a>
		    </p>
                    
                    </div>
                    <div  style="margin-top:6px;bottom: 0;font-weight: bold;">
                        <P style="border: none;bottom: 0;border-top: 1px solid #DDDDDD;padding-bottom: 5px;margin-top: 11px;">
                            <label>Total</label>
                            {$jobsAppraisalRequiredNumber + $jobsBranchRepairNumber + $jobsAuthRequiredNumber + $jobsAwaitingPartsNumber + $jobsInStoreNumber + $jobsWithSupplierNumber + $jobsAwaitingCollectionNumber + $jobsCustomerNotifiedNumber}
                        </P>
                    </div>
		</fieldset>
	    </form>
                
                    <form id="jobsInQueryForm" class="inline span-12 last" style = "position:relative;top:0px;">
		<fieldset style="padding-bottom:5px;">
		    <legend>Branch Jobs in Query</legend>
                    <div id="jobsInQueryFormDiv" >
                        <p style = "border:none; margin-top:5px;">
                            <label style="width:250px; text-align:left; padding-left:5px;">Response Required</label>
                            <a href="#" id="responseRequired">
                                <span class="responseRequiredNum opnJobsNum">{$jobsResponseRequiredNumber}</span>
                                <span class="job-block opnJobsBar" id="responseRequiredBar" style="margin-left:10px; width: {if $jobsResponseRequiredNumber == 0 || $openJobsTotal == 0}0{else}{round(($jobsResponseRequiredNumber/$openJobsTotal) * 100)}{/if}px; background-color: #00B050;">&nbsp;</span>
                            </a>
                        </p>
                    </div>
		</fieldset>
	    </form>
	
		
	    <form id="overdue-jobs-form" class="inline span-12 last" style="padding-bottom:15px; margin-top:5px;">
		<fieldset>
		    <legend class="tooltip" title="Number of days the expected service interval is overdue">Branch Overdue Jobs</legend>
                    <div id="overdue-jobs-form-div" style="margin-top:6px;">
                        <p>
                            <label>1 to 5 Days</label>
                            <a href="#" id="overdue1to5">
                                <span class="opnJobsNum">{$jobsOverdue1to5}</span>
                                <span class="job-block" id="overdue1to5Bar" style="width: {if $jobsOverdue1to5 == 0 || $overdueJobsTotal == 0}0{else}{round(($jobsOverdue1to5/$overdueJobsTotal) * 100)}{/if}px; background-color:#feff00;">&nbsp;</span>
                            </a>
                        </p>
                        <p>
                            <label>6 to 10 Days</label>
                            <a href="#" id="overdue6to10">
                                <span class="opnJobsNum">{$jobsOverdue6to10}</span>
                                <span class="job-block" id="overdue6to10Bar" style="width: {if $jobsOverdue6to10 == 0 || $overdueJobsTotal == 0}0{else}{round(($jobsOverdue6to10/$overdueJobsTotal) * 100)}{/if}px; background-color:#ff6600;">&nbsp;</span>
                            </a>
                        </p>
                        <p>
                            <label>over 10 Days</label>
                            <a href="#" id="overdueOver10">
                                <span class="opnJobsNum">{$jobsOverdueOver10}</span>
                                <span class="job-block" id="overdueOver10Bar" style="width: {if $jobsOverdueOver10 == 0 || $overdueJobsTotal == 0}0{else}{round(($jobsOverdueOver10/$overdueJobsTotal) * 100)}{/if}px; background-color:#ff0000;">&nbsp;</span>
                            </a>
                        </p>
                    </div>
                    <div  style="margin-top:6px;bottom: 0;font-weight: bold;">
                        <P style="border: none;bottom: 0;border-top: 1px solid #DDDDDD;">
                            <label>Total</label>
                            {$jobsOverdue1to5 + $jobsOverdue6to10 + $jobsOverdueOver10}
                        </P>
                    </div>
		</fieldset>
	    </form>
                
	    

			
	    
	    
	    <a name="jobsBookmark"></a>
	    
	    <div id="jobsDiv" style="clear:both; display:none; width:100%; position:relative; float:left; margin-top:5px;">
		
		<fieldset style="padding-top:10px;">
		    <legend id="jobsLegend"></legend>
		    {*
		    <table id="jobsTable" class="browse" style="width:100%;">
			<thead>
			    <tr>
				<th>Date</th>
				<th>Days</th>
				<th>Status</th>
				<th>Customer</th>
				<th>Auth No</th>
				<th>Product Type</th>
				<th>Skyline No</th>
				<th>Service Provider</th>
				<th>Job No</th>
			    </tr>
			</thead>
			<tbody></tbody>
		    </table>
		    *}
		</fieldset>
		
		<fieldset id="updateFieldset">
		    <legend id="updateLegend">Process Selected Job</legend>
		    <div style="margin-top:10px; display:inline-block;">
                        {* added new status awaiting parts by srinvias *}
                        <table id="status">
                            <tr>
                                <td><input type="radio" class="updateStatus" name="updateStatus" value="in_store" /> </td>
                                <td><input type="radio" class="updateStatus" name="updateStatus" value="with_supplier" /> </td>
                                <td><input type="radio" class="updateStatus" name="updateStatus" value="awaiting_parts" /> </td>
                                <td><input type="radio" class="updateStatus" name="updateStatus" value="awaiting_collection" /> </td>
                                <td><input type="radio" class="updateStatus" name="updateStatus" value="customer_notified" /> </td>
                                <td style="width:160px"><input type="radio" class="updateStatus" name="updateStatus" value="closed" /> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td>Pending <br /> Collection </td>
                                <td>In Progress with<br /> Service Provider </td>
                                <td>Awaiting<br /> Parts </td>
                                <td>Returned<br /> to Branch </td>
                                <td>Customer<br /> Notified </td>
                                <td>Returned to <br />Customer / Job Closed </td>
                                <td>
                                <a href="#" id="updateStatusBtn" style="width:110px; color:#fff; margin-top:10px; display:block; float:right;" class="blueBtn">Apply New Status</a> 
                                </td>
                            </tr>    
			</table>
			 {* end of adding  new status awaiting parts by srinvias *}
            
		    </div>
		    
		</fieldset>
		
	    </div>
        
        {/if}
        
    {else}    
        
        <div class="span-9" style="padding-top:30px;" >
        
        <b>Please Note:</b>  You do not need to login to action a product search, however, you will prompted to login if your product search leads to a new job booking.  <br><br>
        You must login  to search for a job record.<br>
        
        <a href="{$_subdomain}/index/index"><img src="{$_subdomain}/css/Skins/skyline/images/header_logo.png" alt="" id="logo" border="0"/></a>

        </div>

        
        
        <div id="login" class="span-14" >
    
                <form id="loginForm" name="loginForm" method="post" action="{$_subdomain}/Login" class="prepend-1 span-14">

                    <fieldset>

                        <legend title="">{$page['Text']['legend']|escape:'html'}</legend>

                       <!-- <p>
                            <span style="display: inline-block; width: 312px; text-align: right; ">
                                <a  href="{$_subdomain}/Login/newUser">{$page['Buttons']['new_user']|escape:'html'}</a>
                            </span>
                        </p> -->


                        <p style="text-align:center" >
                            <label for="name">{$page['Labels']['name']}</label>
                            <input type="text" name="name" id="name" value="" class="text jLabelEnabled" tabIndex="1" />
                           <!-- <span class="right-col">
                                <a  href="{$_subdomain}/Login/lostUsername">{$page['Buttons']['lost_username']|escape:'html'}</a>
                            </span> -->
                        </p>

                        <p style="text-align:center" >
                            <label for="password">{$page['Labels']['password']|escape:'html'}</label>
                            <input type="password" name="password" id="password" value="" class="text jLabelEnabled auto-submit" tabIndex="2" />
                           <!-- <span class="right-col">
                                <a href="#" onclick="javascript: lostPassword();">{$page['Buttons']['lost_password']|escape:'html'}</a>
                            </span> -->
                        </p>


                        <p style="text-align:center" >
                            <select name="branch" id="branch" class="text" tabIndex="3"   >
                                <option value="" >{$page['Text']['select_branch']|escape:'html'}</option>
                                {foreach $branches as $b}
				    <option value="{$b.BranchID}" {if $b.BranchID eq $branch}selected{/if}>{$b.BranchName|escape:'html'}</option>
                                {/foreach}
                            </select>



                        </p>


                        {if $error eq ''}
                        <p id="error" class="formError" style="display: none;text-align:center" />
                        {else}
                        <p style="text-align:center" id="error" class="formError" >{$error|escape:'html'}</p>
                        {/if}

                        <p style="text-align:center" >
                            <span style="display: inline-block; text-align: center;" >
                                <a href="javascript:if (validateForm()) document.loginForm.submit();" tabIndex="4">{$page['Buttons']['submit']|escape:'html'}</a>
                                
                                 &nbsp;&nbsp;
                        
                                <a href="#" onclick="document.location.href='{$_subdomain}/index/index'; return false;"  >{$page['Buttons']['cancel']|escape:'html'}</a>
                        
                            </span>
                           
                        </p>

                    </fieldset>

                </form>

        </div>
        
        
        
    {/if}
    
    
                                    
</div>
{/block}
