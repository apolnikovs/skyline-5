{if $accessErrorFlag eq true} 
    
<div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
            <p>
                <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>
            </p>
            <p>
                <span class= "bottomButtons" >
                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >
                </span>
            </p>
        </fieldset>   
    </form>            
</div>    
    
{else}  
    
<script type="text/javascript" >
$(document).ready(function() {  

               
    /* =======================================================
     *
     * Initialise input auto-hint functions...
     *
     * ======================================================= */
    
    $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } );  
                
         }); 
         
    
        
</script>
    
<div id="JobUpdateContactNotePanel" class="SystemAdminFormPanel" >   
    <form id="JobUpdateContactNoteForm" name="JobUpdateContactNoteForm" method="post" action="#" class="inline" >
        <fieldset>
            <legend title="" >{$page['Text']['add_contact_notes_legend']|escape:'html'}</legend>
            <p><label id="suggestText"></label></p>
            <p>
                {* <label class="fieldLabel" for="ContactHistoryActionID" style="width: 10px;"><sup>*</sup></label> *}
                <select name="ContactHistoryActionID" id="ContactHistoryActionID" class="text" style="width: 540px">
                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>
                    {foreach $contact_history_actions as $contact_history_action}
                        append<option value="{$contact_history_action.ContactHistoryActionID}">{$contact_history_action.Action|escape:'html'}</option>
                    {/foreach}
                </select>
            </p>
            <p>
                {* <label class="fieldLabel" style="width: 10px;"><sup>*</sup></label> *}
                <textarea id="Note" name="Note" style="width: 540px" class="auto-hint" title = "{$page['Labels']['contact_history_note']|escape:'html'}"></textarea>
            </p>
            <p>
                <input type="hidden" id="JobID" name="JobID" value="{$JobID}">
                <span class= "bottomButtons" >
                    <input type="submit" name="insert_save_btn_notes" class="textSubmitButton" id="insert_save_btn_notes" style="margin-right:10px;" value="{$page['Buttons']['save']|escape:'html'}" >
                    <input type="submit" name="cancel_btn_notes" class="textSubmitButton" id="cancel_btn_notes" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}">
                </span>
            </p>
        </fieldset>    
    </form>            
</div>
                 
{/if}                                     
                                    
{* This block of code is for to display message after data update *} 
                                  
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" >{$page['Text']['add_contact_notes_legend']|escape:'html'}</legend>
            <p>
                <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
            </p>
            <p>
                <span class= "bottomButtons" >
                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >
                </span>
            </p>
     </fieldset>   
    </form>               
</div>    

{* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" >{$page['Text']['add_contact_notes_legend']|escape:'html'}</legend>
                <p>
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                </p>
                <p>
                    <span class= "bottomButtons" >
                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >
                    </span>
                </p>
        </fieldset>   
    </form>               
</div>                            