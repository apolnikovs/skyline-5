<?php

require_once('CustomModel.class.php');

/**
* Description
*
* This class is used for handling database actions of UnitTypes Page in Product Setup section under System Admin
*
* @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
* @version     1.0
*/

class UnitTypes extends CustomModel {
    
    private $conn;
    
    private $table = "unit_type";
    private $table_unit_client_type = "unit_client_type";
    private $table_repair_skill = "repair_skill";
    private $tables = "unit_type AS T1 LEFT JOIN repair_skill AS T2 ON T1.RepairSkillID=T2.RepairSkillID";
    private $dbTableColumns = [ 'T1.UnitTypeID', 
			        'T1.UnitTypeName',				
			        'T2.RepairSkillName', 
			        'T1.Status'
			      ];
    private $dbTablesColumns = ['T1.UnitTypeID', 
				'T1.UnitTypeName', 				
				'T2.RepairSkillName', 
				'T1.Status', 
				'T1.UnitTypeID AS Assigned'
			       ];   

    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
    /**
    * Description
    * 
    * This method is for fetching data from database
    * 
    * @param array $args Its an associative array contains where clause, limit and order etc.
    * @global $this->conn
    * @global $this->table 
    * @global $this->table_unit_client_type
    * @global $this->table_repair_skill 
    * 
    * @global $this->dbColumns
    * @return array 
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */  
    
    public function fetch($args) {
        
        $ClientID = isset($args['secondArg']) ? $args['secondArg'] : '';
        $showAssigned = isset($args['thirdArg']) ? $args['thirdArg'] : '';
	
        if($ClientID != '') {
	    
	    $q = "  SELECT	unit_type.UnitTypeID		AS '0',
				unit_type.UnitTypeName 		AS '1',
				
				CASE
				    WHEN unit_client_type.UnitTurnaroundTime 
				    THEN unit_client_type.UnitTurnaroundTime 
				    ELSE ''
				END				AS '2',
				
				repair_skill.RepairSkillName 	AS '3',
			
				CASE
				    WHEN unit_type.Status
				    THEN unit_type.Status
				    ELSE 'In-active'
				END				AS '4',	    
			
			
				CASE
				    WHEN unit_client_type.Status = 'Active'

				    THEN CONCAT('<input class=\'checkBoxDataTable\' type=\'checkbox\' checked=\'checked\' value=\'',
						unit_type.UnitTypeID,
						'\' name=\'assignedUnitTypes[]\'>')

				    ELSE CONCAT('<input class=\'checkBoxDataTable\' type=\'checkbox\' value=\'',
						unit_type.UnitTypeID,
						'\' name=\'assignedUnitTypes[]\'>')
				END				AS '5'
                   			
		    FROM	unit_type

		    LEFT JOIN	unit_client_type ON unit_type.UnitTypeID = unit_client_type.UnitTypeID AND unit_client_type.ClientID = :clientID
		    LEFT JOIN   repair_skill ON unit_type.RepairSkillID = repair_skill.RepairSkillID
	    ";
	    
            if($showAssigned) {
		$q .= " WHERE unit_client_type.Status = 'Active'";		
		
		/*
		$dbTablesColumns2 = [	'T1.UnitTypeID', 
					'T1.UnitTypeName',
					'T3.RepairSkillName', 
					'T1.Status', 
					'T1.UnitTypeID AS Assigned'
				    ];
		
		$dbTables = $this->table . " AS T1 LEFT JOIN " . $this->table_unit_client_type . " AS T2 ON T1.UnitTypeID=T2.UnitTypeID LEFT JOIN " . $this->table_repair_skill . " AS T3 ON T1.RepairSkillID = T3.RepairSkillID";
                $args['where'] = "T2.ClientID = '" . $ClientID . "' AND T2.Status = '" . $this->controller->statuses[0]['Code'] . "'";
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns2, $args);
		*/
		
            } else { 
                //$output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTablesColumns, $args);
            }
	    
	    if($args["sSearch"] != "") {
		$s = $args["sSearch"];
		$q .= "	WHERE	( 
				    unit_type.UnitTypeName LIKE '%$s%' ||
				    repair_skill.RepairSkillName LIKE '%$s%' ||
				    unit_type.Status LIKE '%$s%'
				)
			";
	    }
	    
	    
	    $values = ["clientID" => $ClientID];
	    
            
	    $result = $this->query($this->conn, $q, $values);
	    $total = count($result);
	    
	    $q .= " LIMIT " . $args["iDisplayStart"] . ", " . $args["iDisplayLength"];
	    
	    $result = $this->query($this->conn, $q, $values);
	    
	    $output = (object)[ 
				"sEcho" => (int)$args["sEcho"], 
				"iTotalRecords" => $total, 
				"iTotalDisplayRecords" => $total,
				"aaData" => $result
			      ];
	    
        } else {
             
	    $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns, $args);
	     
        }
        
        return  $output;
        
    }
    

    
    /**
    * Description
    * 
    * This method calls update method if the $args contains primary key.
    * 
    * @param array $args Its an associative array contains all elements of submitted form.
    * @return array It contains status and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
    */   
    
    public function processData($args) {

	if(!isset($args['UnitTypeID']) || !$args['UnitTypeID']) {
	    return $this->create($args);
	} else {
	    return $this->update($args);
	}
	
    }
    
    
      /**
     * Description
     * 
     * This method is used for to check whether Unit Type Name exists.
     *
     * @param interger $UnitTypeID  
     * @param interger $UnitTypeName
     
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($UnitTypeID, $UnitTypeName) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT UnitTypeID FROM '.$this->table.' WHERE UnitTypeName=:UnitTypeName AND UnitTypeID!=:UnitTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':UnitTypeID' => $UnitTypeID,  ':UnitTypeName' => $UnitTypeName));
        $result = $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['UnitTypeID']) {
                return false;
        }
        
        return true;
    
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to check whether client is exists for the Unit Type.
     *
     * @param interger $ClientID 
     * @param interger $UnitTypeID
     * 
     * @global $this->table_unit_client_type
     
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isClientExists($ClientID, $UnitTypeID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT UnitClientTypeID FROM '.$this->table_unit_client_type.' WHERE UnitTypeID=:UnitTypeID AND ClientID=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':UnitTypeID' => $UnitTypeID, ':ClientID' => $ClientID));
        $result = $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['UnitClientTypeID'])
        {
                return true;
        }
        
        return false;
    
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {
        
        if($this->isValid(0, $args['UnitTypeName'])) {  
	    
	    $result = false;
                    
	    /* Execute a prepared statement by passing an array of values */
	    $sql = 'INSERT INTO	' . $this->table . ' 
				(
				    UnitTypeName, 
				    RepairSkillID, 
				    Status, 
				    ImeiRequired,
				    IMEILengthFrom,
				    IMEILengthTo,
                                    DOPWarrantyPeriod, 
                                    ProductionDateWarrantyPeriod,
				    CreatedDate, 
				    ModifiedUserID, 
				    ModifiedDate
				)
		    VALUES
				(
				    :UnitTypeName, 
				    :RepairSkillID, 
				    :Status,
				    :ImeiRequired,
				    :IMEILengthFrom, 
				    :IMEILengthTo,
                                    :DOPWarrantyPeriod, 
                                    :ProductionDateWarrantyPeriod,
				    :CreatedDate, 
				    :ModifiedUserID, 
				    :ModifiedDate
				)
		   ';

	    $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

	    $result = $insertQuery->execute([
		':UnitTypeName' => $args['UnitTypeName'],
		':RepairSkillID' => ($args['RepairSkillID'] == '') ? NULL : $args['RepairSkillID'], 
		':Status' => $args['Status'],
		":ImeiRequired" => $args["ImeiRequired"],
		':IMEILengthFrom' => $args['IMEILengthFrom'],  
		':IMEILengthTo' => $args['IMEILengthTo'],      
                ":DOPWarrantyPeriod" => $args["DOPWarrantyPeriod"],
                ":ProductionDateWarrantyPeriod" => $args["ProductionDateWarrantyPeriod"],
                ':CreatedDate' => date("Y-m-d H:i:s"),
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s")
	    ]);

	    if($result) {
                
                $args['UnitTypeID'] = $this->conn->lastInsertId();
                
                $this->processAccessories($args);
                
		return	[
			    'status' => 'OK',
			    'message' => $this->controller->page['Text']['data_inserted_msg']
			];
	    } else {
		return	[
			    'status' => 'ERROR',
			    'message' => $this->controller->page['Errors']['data_not_processed']
			];
	    }
	    
        } else {
	    
             return [
			'status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)
		    ];
	     
        }
	
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to process Accessories (insert/update).
     *
     * @param  array $args
    
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function processAccessories($args)
    {
        
        if(isset($args['UnitTypeID']))
        {
             /* Execute a prepared statement by passing an array of values */
            $sql = 'DELETE FROM unit_type_accessory WHERE UnitTypeID=:UnitTypeID';
            $deleteQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $deleteQuery->execute(array(':UnitTypeID' => $args['UnitTypeID']));
            
            
            
            
             /* Execute a prepared statement by passing an array of values */
	    $sql2 = 'INSERT INTO	unit_type_accessory
				(
				    UnitTypeID, 
				    AccessoryID
				)
		    VALUES
				(
				    :UnitTypeID, 
				    :AccessoryID
				)
		   ';

	    $insertQuery = $this->conn->prepare($sql2, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

            
            if(isset($args['AccessoryID']) && is_array($args['AccessoryID']))
            {    
                foreach($args['AccessoryID'] as $aID)
                {
                    $result = $insertQuery->execute([
                        ':UnitTypeID' => $args['UnitTypeID'],
                        ':AccessoryID' => $aID
                    ]);
                }
            }

        }    
        
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
     * @global $this->table_unit_client_type
     
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT UnitTypeID, UnitTypeName, Status, ImeiRequired, IMEILengthFrom, IMEILengthTo, RepairSkillID, DOPWarrantyPeriod, ProductionDateWarrantyPeriod FROM '.$this->table.' WHERE UnitTypeID=:UnitTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':UnitTypeID' => $args['UnitTypeID']));
        $result = $fetchQuery->fetch();
        
        
        if(isset($result['UnitTypeID']))
        {
            $result['AccessoryID'] = array();
            
            
            $sql2 = 'SELECT AccessoryID FROM unit_type_accessory WHERE UnitTypeID=:UnitTypeID';
            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            $fetchQuery2->execute(array(':UnitTypeID' => $result['UnitTypeID']));
            
            while($aID = $fetchQuery2->fetch())
            {
               $result['AccessoryID'][] =  $aID[0];
            }    
            
        }    
        
        return $result;
     }
     
     
     
     
     
     
      /**
     * Description
     * 
     * This method is used for to fetch accessories from database for given unit type id.
     *
     * @param  int $UnitTypeID
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchAccessories($UnitTypeID) {
        
            $sql2 = 'SELECT T1.AccessoryID, T2.AccessoryName FROM unit_type_accessory AS T1 LEFT JOIN accessory AS T2 ON T1.AccessoryID=T2.AccessoryID WHERE T1.UnitTypeID=:UnitTypeID AND T2.Status=:Status';
            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            $fetchQuery2->execute(array(':UnitTypeID' => $UnitTypeID, ':Status' => 'Active'));

            $result = $fetchQuery2->fetchAll();


            return $result;
     }
     
     
     
     
     
    
     
     /**
     * Description
     * 
     * This method is used for to assign client to selected unit Type.
     *
     * @param int    $ClientID
     * @param array  $assignedUnitTypes
     * @param string $sltUnitTypes     
     
     * @global $this->table_unit_client_type
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updateClientUnitTypes($ClientID, $assignedUnitTypes, $sltUnitTypes){
        
             $result = false;
        
            
            //Updating details into $this->table_unit_client_type
            $sql2 = '	UPDATE	' . $this->table_unit_client_type . ' 
		
			SET	EndDate = :EndDate, 
				Status = :Status, 
				ModifiedUserID = :ModifiedUserID, 
				ModifiedDate = :ModifiedDate 
				
			WHERE	ClientID = :ClientID AND UnitTypeID = :UnitTypeID';

            /* Execute a prepared statement by passing an array of values */    
            $updateQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            //Inserting details into $this->table_unit_client_type
            /* Execute a prepared statement by passing an array of values */
            $sql3 = 'INSERT INTO '.$this->table_unit_client_type.' (ClientID, UnitTypeID, EndDate, CreatedDate, Status, ModifiedUserID, ModifiedDate)
            VALUES(:ClientID, :UnitTypeID, :EndDate, :CreatedDate, :Status, :ModifiedUserID, :ModifiedDate)';

            $insertQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


           
            $sltUnitTypesList = explode(",", $sltUnitTypes);
            
           
            if(is_array($sltUnitTypesList))
            {
                    if(!is_array($assignedUnitTypes))
                    {   
                        $assignedUnitTypes = array();
                    }
                
                    foreach ( $sltUnitTypesList as $autKey=>$autValue)
                    {

                        if($autValue)
                        {    
                                $existsResult = $this->isClientExists($ClientID, $autValue);



                                $EndDate = date("Y-m-d H:i:s");
                                $args['Status'] = $this->controller->statuses[1]['Code'];

                                //If the assigned check box is checked then we are assigning active as status and end date to null.
                                 
                                if(in_array($autValue, $assignedUnitTypes))
                                {
                                    $EndDate = "0000-00-00 00:00:00";
                                    $args['Status'] = $this->controller->statuses[0]['Code'];
                                }
                                        
                                $result = true;
                                if($existsResult)
                                {
                                    $result = $updateQuery2->execute(array(

                                    ':ClientID' => $ClientID, 
                                    ':UnitTypeID' => $autValue,    
                                    ':EndDate' => $EndDate,
                                    ':Status' => $args['Status'],
                                    ':ModifiedUserID' => $this->controller->user->UserID,
                                    ':ModifiedDate' => date("Y-m-d H:i:s")

                                    ));
                                }
                                else if($args['Status']!=$this->controller->statuses[1]['Code'])//If the relation is not exists in database and assigned checkbox is ticked then we are inserting relation into database.
                                {

                                        $result = $insertQuery3->execute(array(

                                        ':ClientID' => $ClientID, 
                                        ':UnitTypeID' => $autValue,    
                                        ':EndDate' => $EndDate,    
                                        ':CreatedDate' => date("Y-m-d H:i:s"),
                                        ':Status' => $args['Status'],
                                        ':ModifiedUserID' => $this->controller->user->UserID,
                                        ':ModifiedDate' => date("Y-m-d H:i:s")

                                        ));

                                }
                            
                        }   

                    }
            
            }
        
        return $result;
    
    }
     
     
     
    /**
    * Description
    * 
    * This method is used for to udpate a row into database.
    *
    * @param array $args
    * @global $this->table 
    * @global $this->table_unit_client_type
    *    
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function update($args) {
        
        if($this->isValid($args['UnitTypeID'], $args['UnitTypeName'])) {        
	    
	    $EndDate = "0000-00-00 00:00:00";
	    
	    $row_data = $this->fetchRow($args);
	    
	    if($this->controller->statuses[1]['Code'] == $args['Status']) {
		if($row_data['Status']!=$args['Status']) {
		    $EndDate = date("Y-m-d H:i:s");
		}
	    }

	    /* Execute a prepared statement by passing an array of values */
	    $sql = 'UPDATE  ' . $this->table . ' 
		
		    SET	    UnitTypeName = :UnitTypeName, 
			    RepairSkillID = :RepairSkillID, 
			    Status = :Status, 
			    ImeiRequired = :ImeiRequired,
			    IMEILengthFrom = :IMEILengthFrom,
			    IMEILengthTo = :IMEILengthTo,
                            DOPWarrantyPeriod = :DOPWarrantyPeriod,
                            ProductionDateWarrantyPeriod = :ProductionDateWarrantyPeriod,
			    EndDate = :EndDate, 
			    ModifiedUserID = :ModifiedUserID, 
			    ModifiedDate = :ModifiedDate 
			    
		    WHERE   UnitTypeID = :UnitTypeID';

	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    
	    $result = $updateQuery->execute([
		':UnitTypeName' => $args['UnitTypeName'],
		':RepairSkillID' => ($args['RepairSkillID'] == '') ? NULL : $args['RepairSkillID'],
		':Status' => $args['Status'],
		":ImeiRequired" => $args["ImeiRequired"],
		':IMEILengthFrom' => $args['IMEILengthFrom'], 
		':IMEILengthTo' => $args['IMEILengthTo'],    
                ":DOPWarrantyPeriod" => $args["DOPWarrantyPeriod"],
                ":ProductionDateWarrantyPeriod" => $args["ProductionDateWarrantyPeriod"],
                ':EndDate' => $EndDate,
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s"),
		':UnitTypeID' => $args['UnitTypeID']  
	    ]);
               
	    if($result) {
                
                $this->processAccessories($args);
                
		return	[
			    'status' => 'OK',
			    'message' => $this->controller->page['Text']['data_updated_msg']
			];
	    } else {
		return	[
			    'status' => 'ERROR',
			    'message' => $this->controller->page['Errors']['data_not_processed']
			];
	    }
              
        } else {
	    
	    return  [
			'status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)
		    ];
	    
        }
	
    }
    
    
    
    public function delete() {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
  
    
    /**
    Returns all unit types
    2012-01-24 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getAllUnitTypes() {
	
	$q = "SELECT * FROM unit_type order by UnitTypeName";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    
    
    
    /**
     * getId
     *  
     * Get the UnitTypeID from the name of a Unit Type
     * 
     * @param string $utName    The name of the unit type
     * 
     * @return integer  Unit Type ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getId($utName) {
        $sql = "
                SELECT
			`UnitTypeID`
		FROM
			`unit_type`
		WHERE
			`UnitTypeName` = '$utName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['UnitTypeID']);                                   /* Unit Type exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
        public function getName($id) {
        $sql = "SELECT UnitTypeName FROM unit_type WHERE UnitTypeID=$id";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['UnitTypeName']);                                   /* Unit Type exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
  
     /*
     *  Updated By Praveen Kumar 
      * check IMEILengthFrom & IMEILengthTo values from model table
     */
    
    public function isImeiRequired($unitTypeID, $modelName = '') {
	
	if($modelName != "") {
	    
	    $q = "SELECT IMEILengthFrom,IMEILengthTo 
			FROM model 
			WHERE 
			ModelNumber = :modelName";  
			    
		
		$values = ["modelName" => $modelName];
		$modelResult = $this->query($this->conn, $q, $values);	
		//$this->log($modelName);
		//$this->log($modelResult);
		
		
	   
	} 
	
	$q = "SELECT * FROM unit_type WHERE UnitTypeID = :unitTypeID";
		$values = ["unitTypeID" => $unitTypeID];
		$result = $this->query($this->conn, $q, $values);	
		if(isset($result[0]["ImeiRequired"]) && $result[0]["ImeiRequired"] == "Yes") {
	    	    
		    if(isset($modelResult[0]['IMEILengthFrom']) && $modelResult[0]['IMEILengthFrom'] &&  isset($modelResult[0]['IMEILengthTo']) && $modelResult[0]['IMEILengthTo'])
		    {
			$result[0]['IMEILengthFrom'] = $modelResult[0]['IMEILengthFrom'];
			$result[0]['IMEILengthTo'] = $modelResult[0]['IMEILengthTo'];
		    }
		 return $result[0];
	   
		} else {
			return false;
	    }
	
    }  
	
        
}
?>