<?php

require_once('Constants.class.php');

/*******************************************************************
 * Short Description of Functions.
 * 
 * Long description of Functions.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.4
 *  
 * Changes
 * Date        Version Author                Reason
 * 30/01/2013  1.0     Brian Etherington     Initial Version
 * 11/03/2013  1.1     Brian Etherington     Added UserTypeFromObject
 *                                                 UserJobFilter
 *                                                 ActiveJobFilter
 *                                                 CancelledJobFilter
 * 12/04/2013  1.2     Brian Etherington     Added CreateGuid
 * 23/04/2013  1.3     Brian Etherington     Add str_putcsv
 * 07/05/2013  1.4     Brian etherington     Add getIPAddress
 ********************************************************************/

class Functions {
    
    static public function getIPAddress() {
        if(!empty($_SERVER['HTTP_CLIENT_IP']))   
            return $_SERVER['HTTP_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        return $_SERVER['REMOTE_ADDR'];
    }
    
    static public function CreateGuid() {
        // this is a poor man's GUID generator
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', 
                         mt_rand(0, 65535), mt_rand(0, 65535), 
                         mt_rand(0, 65535), mt_rand(16384, 20479), 
                         mt_rand(32768, 49151), mt_rand(0, 65535), 
                         mt_rand(0, 65535), mt_rand(0, 65535));   
    }
    
    static public function str_putcsv($value, $delimiter = ',', $enclosure = '"') {
        
        if (trim($value) == '') return '';
        
	// check for presence of special char.
        if ((strpos($value, $delimiter)  !== false) || (strpos($value, $enclosure)  !== false) ||
            (substr($value, 0, 1)  == ' ') || (substr($value, -1, 1)  == ' ') ||
            (strpos($value, "\t") !== false) || (strpos($value, "\n") !== false) || (strpos($value, "\r") !== false)) {
            // if present surround the value in quotes and replace already existing " with ""
            return $enclosure . str_replace($enclosure, $enclosure.$enclosure, $value) . $enclosure;
        }
        
        // no encoding necessary - just return $value
        return $value;
    }
    
    static public function hasPermission($user, $id, $level="R") {
        
        if ($user->UserType == 'Admin' || (isset($user->Permissions[$id]) && strpos($user->Permissions[$id],$level) !== false))
            return true;
        
        return false;
    }
    
    static public function UserTypeFromObject( stClass $user ) {
        return Functions::UserType( $user.SuperAdmin, $user.NetworkID, 
                                    $user.ServiceProviderID, $user.ClientID, 
                                    $user.BranchID, $user.ManufacturerID,
                                    $user.ExtendedWarrantorID );
    }
   
    static public function UserType( $SuperAdmin, $NetworkID, $ServiceProviderID, $ClientID, 
                                     $BranchID, $ManufacturerID, $ExtendedWarrantorID ) {
               
            if ( $SuperAdmin != null) {
                return Constants::SuperAdmin; 
            } else if ( $ServiceProviderID != null ) {
                return Constants::ServiceProvider; 
            } else if ( $ManufacturerID != null ) {
                return Constants::Manufacturer; 
            } else if ( $ExtendedWarrantorID != null ) {
                return Constants::ExtendedWarrantor; 
            } else if ( $BranchID != null ) {
                return Constants::Branch;
            } else if ( $ClientID != null ) {
                return Constants::Client; 
            } else if ( $NetworkID != null ) {
                return Constants::Network;
            }
        
            return Constants::UnknownUser;
    }
    
    static public function UserJobFilter( $user, $jobalias='job', $branchbrandalias='branch_brand' ) {
        
        if ( $user === null || !isset($user->UserType)) return '';
             
        switch ($user->UserType) { /* Set job filter based on user type */
            case 'Admin':
                return '' ;
                break;
            
            case 'ServiceProvider':
                return ' AND '.$jobalias.'.ServiceProviderID='.$user->ServiceProviderID;
                break;
            
            case 'Branch':
                switch ($user->BranchJobSearchScope) { /* User is branch so filter is based on Branch Job Scope Search */
                    case 'Brand':
                        return (" AND $branchbrandalias.`BrandID` = {$user->DefaultBrandID}");
                        break;
                    
                    case 'Client':
                        return ' AND '.$jobalias.'.ClientID='.$user->ClientID;
                        break;
                    
                    default: /* Branch Only */
                        return ' AND '.$jobalias.'.BranchID='.$user->BranchID;
                }                
                break;
            
            case 'Client':
                return ' AND '.$jobalias.'.ClientID='.$user->ClientID;
                break;
            
            case 'Network':
                return ' AND '.$jobalias.'.NetworkID='.$user->NetworkID;
                break;
            
            case 'Manufacturer':
                return ' AND '.$jobalias.'.ManufacturerID='.$user->ManufacturerID;
                break;
            
             case 'ExtendedWarrantor':
                return '';
                break;
            
            default:
                throw new exception('Unrecognised User Type');
        }   
    }
    /*
     * Added line for "'29A JOB CANCELLED - NO CONTACT'" - Soubhik
     */
    static public function ActiveJobFilter( $statusalias='status') {
        return " AND ".$statusalias.".StatusName != '29 JOB CANCELLED' AND ".$statusalias.".StatusName != '29A JOB CANCELLED - NO CONTACT'";
    }
    
    static public function CancelledJobFilter( $statusalias='status') {
        return " AND ".$statusalias.".StatusName='29 JOB CANCELLED' OR ".$statusalias.".StatusName = '29A JOB CANCELLED - NO CONTACT'";
    }
       
    static public function FormatAddress( $BuildingNameNumber, $Street, $LocalArea,
                                          $TownCity, $County, $PostalCode, $Country,
                                          $style='SingleLine') {
                     
        switch ($style) {
            
            case 'SingleLine':
                
                $result = '';
        
                if (!empty($BuildingNameNumber)) {
                    $result .= trim($BuildingNameNumber);
                    $result .= is_numeric(substr($result, 0, 1)) ? ' ' : ', ';
                }
        
                if (!empty($Street)) {
                    $result .= trim($Street) . ', ';
                }
        
                if (!empty($LocalArea)) {
                    $result .= trim($LocalArea) . ', ';
                }
        
                if (!empty($TownCity)) {
                    $result .= trim($TownCity) . ', ';
                }

                if (!empty($County)) {
                    $result .= trim($County) . ', ';
                }
        
                if (!empty($PostalCode)) {
                    $result .= trim($PostalCode) . ', ';
                }
        
                if (!empty($Country)) {
                    $result .= trim($Country);
                }
        
                return trim($result);
            
            case 'Array':
                
                $result = array(); 
                
                $row = 1;
                
                if (!empty($BuildingNameNumber)) {
                    $line_1 = trim($BuildingNameNumber);
                    if (is_numeric(substr($line_1, 0, 1))) {
                        $line_1 .= ' ';
                        $line_1 .= $Street;
                        $result['line_1'] = trim($line_1);
                        $row = 2;
                    } else {
                        $result['line_1'] = $line_1;
                        if (empty($Street)) {
                            $row = 2;
                        } else {
                            $result['line_2'] = trim($Street);
                            $row = 3;
                        }
                        
                    }
                } else {
                    if (!empty($street)) {
                        $result['line_1'] = trim($Street);
                        $row = 2;
                    }
                }
        
         
                if (!empty($LocalArea)) {
                    $result['line_'.$row] = trim($LocalArea);
                    $row++;
                }
        
                if (!empty($TownCity)) {
                    $result['line_'.$row] = trim($TownCity);
                    $row++;
                }
                
                while($row < 5) {
                    $result['line_'.$row] = '';
                    $row++;
                }
                
                $result['building'] = empty($BuildingNameNumber) ? '' : trim($BuildingNameNumber);
                $result['street'] = empty($Street) ? '' : trim($Street);
                $result['area'] = empty($LocalArea) ? '' : trim($LocalArea);
                $result['town'] = empty($TownCity) ? '' : trim($TownCity);
                $result['county'] = empty($County) ? '' : trim($County);    
                $result['postcode'] = empty($PostalCode) ? '' : trim($PostalCode);
                $result['country'] = empty($Country) ? '' : trim($Country);
        
                return $result;               
                break;
        }
        
        return '';
        

    }
}
